import re
import flask
import json
import os
from flask_login import current_user
from .utils import markdown_to_html, markdown_to_inline_html, convert_keys_to_int
from .models.learning_resource import LearningResource, UserResourceFeedback
from .app import db


class Assignment:
    # TODO: implement this as a collection of jinja2 macros

    def __init__(self, assignment, node: dict):
        """Args:
            assignment (dict): Version-specific assignment data structure.
            node (dict): Node info.
        """
        assert node["format_version"][0] == 1
        self.assignment = assignment
        self.node: dict = node

    @staticmethod
    def load_from_directory(directory):
        with open(f"{directory}/assignment.json") as file:
            assignment = convert_keys_to_int(json.loads(file.read()))
        with open(f"{directory}/node.json") as file:
            node: dict = convert_keys_to_int(json.loads(file.read())) # type: ignore
        return Assignment(assignment, node)

    @property
    def passing_grade(self):
        return 6 if "ects" in self.node else 8

    @property
    def goals(self):
        return self.node["goals"]

    def to_ai_markdown(self, objectives=True, depth=1):
        if objectives is True:
            objectives = self.assignment["document"]

        if objectives is None:
            return ''

        out = ''
        for objective in objectives:
            if isinstance(objective, str):
                out += objective
            elif "rubric" in objective:
                pos = objective["rubric"]
                rubric = self.assignment["rubrics"][pos]
                out += f'\n\n{"#"*depth} Objective #{pos+1}{": "+rubric["title"] if "title" in rubric else ""}\n{rubric["text"] or ""}'
            elif "link" in objective:
                pass
            else: # it's a section
                if 'title' in objective:
                    out += f'\n\n{"#"*depth} {objective["title"]}'
                out += f'\n{self.to_ai_markdown(objective["children"], depth+1)}'
        return out
        

    def render(self, show_rubrics, grading=None, attempt=None, ai_feedback=None):
        """Args:
            show_rubrics: True => show rubrics, 'disabled' => shown but disabled, False => 'not shown'
            grading: An optional Grading object containing `grade`, `motivation`, `objective_scores`
                and `objective_motivations`.
            attempt: An optional Attempt object containing `done_objectives` and `alternatives`. Alternatives is an dictionary containing the chosen alternatives.
            If not specified, all alternatives will be rendered. For example:
            `{"proto": "udp", "syntax": "c", "feature1": "functions"}
        """
        
        # Destructure attempt or provide standard values
        return \
            self.render_requirements(self.assignment["document"], show_rubrics, grading, attempt, ai_feedback=ai_feedback) + \
            self.render_grading(self.assignment, self.goals, show_rubrics, grading) + \
            '<script src="/static/toggle_objective.js"></script>'

    ALTERNATIVES_REGEX = r'''\[([a-zA-Z0-9_]+):([a-zA-Z0-9_]+)\]{(.*?)}'''

    @staticmethod
    def render_text(text, alternatives, inline=False):
        if text is None:
            return ""
        def replace(m):
            if m.group(1) in alternatives:
                return m.group(3) if alternatives[m.group(1)] == m.group(2) else ''
            return f'<span class="alternative"><span class="name">{m.group(1)}:{m.group(2)}</span>{m.group(3)}</span>'
        text = re.sub(Assignment.ALTERNATIVES_REGEX, replace, text)
        return markdown_to_inline_html(text) if inline else markdown_to_html(text)

    def render_objective(self, rubric, tag, pos, attempt):
        done_objectives = attempt.done_objectives if attempt else []
        alternatives = attempt.alternatives if attempt else {}
        checked = pos in done_objectives

        show_checkbox = current_user.is_authenticated and current_user.is_student and current_user.current_attempt == attempt

        checkbox = f"""<input class="checkbox" onclick="toggle_objective(this)" data-pos="{pos}" type="checkbox" {'checked' if checked else ''}/>"""

        return '<div class="objective">' +\
            '<section class="text rows1 notification">' +\
            f'<h3>{checkbox if show_checkbox else ""}Objective #{pos+1}{": "+rubric["title"] if "title" in rubric else ""}{tag}</h3>' +\
            f'<div class="wrapper {"completed-objective" if checked and show_checkbox else ""}" >' +\
            (self.render_text(rubric["text"], alternatives) if "text" in rubric else "") +\
            '</div>' +\
            '</section>'

    def get_alternatives(self):
        alternatives = {}
        self.find_alternatives_recursive(alternatives, self.assignment["document"])
        return alternatives

    def find_alternatives_recursive(self, alternatives, objectives):
        if objectives == None:
            return

        for objective in objectives:
            if isinstance(objective, str):
                self.find_alternatives_text(objective, alternatives)

            elif "rubric" in objective:
                pos = objective["rubric"]
                rubric = self.assignment["rubrics"][pos]
                if 'text' in rubric:
                    self.find_alternatives_text(rubric['text'], alternatives)
                if 'title' in rubric:
                    self.find_alternatives_text(rubric['title'], alternatives)

            elif "link" in objective:
                pass

            else: # it's a section
                if "title" in objective:
                    self.find_alternatives_text(objective["title"], alternatives)
                self.find_alternatives_recursive(alternatives, objective["children"])

    @staticmethod
    def find_alternatives_text(text, alternatives):
        for match in re.finditer(Assignment.ALTERNATIVES_REGEX, text):
            if match.group(1) not in alternatives:
                alternatives[match.group(1)] = []
            alternatives[match.group(1)].append(match.group(2))

    def render_requirements(self, objectives, show_rubrics, grading, attempt, depth=1, ai_feedback=None):
        if objectives == None:
            return ''

        alternatives = attempt.alternatives if attempt else {}

        disabled = "disabled " if show_rubrics == "disabled" or show_rubrics is False else ""
        out = ''
        for objective in objectives:  # noqa: PLR1702
            if isinstance(objective, str):
                out += self.render_text(objective, alternatives)
            elif "rubric" in objective:
                pos = objective["rubric"]
                rubric = self.assignment["rubrics"][pos]
                score = grading and pos < len(grading.objective_scores) and grading.objective_scores[pos]
                motivation = grading.objective_motivations[pos] if grading and pos < len(grading.objective_motivations) else ''
                if "range" in rubric:
                    points = rubric["range"][1] - rubric["range"][0]
                    bonus = points * rubric.get("bonus", 0)
                    malus = points * rubric.get("malus", 0)
                    points -= bonus + malus
                    tag = ""
                    if bonus:
                        tag += f'<span class="tag is-info">+ {self.format_grade(bonus)} points</span>'
                    if malus:
                        tag += f'<span class="tag is-warning">- {self.format_grade(malus)} points</span>'
                    if points:
                        tag += f'<span class="tag">{self.format_grade(points)} points</span>'
                else:
                    tag = '<span class="tag is-important">MUST</span>'

                out += self.render_objective(rubric, tag, pos, attempt)

                if show_rubrics or grading:
                    out += '<div class="rubric notification is-important rows1">'
                    if show_rubrics:
                        if "range" in rubric: # A numeric rubric
                            if rubric.get("scale")==10 or (score is not None and score not in [0,1,2,3,4]):
                                # Scale [1..10]
                                score10 = "" if score is None else str(round(score/4.0*9+1,1)).replace(".0","")
                                out += f'<input class="input" {disabled}required placeholder="1 - 10" type="number" step="1" min="1" max="10" value="{score10}" name="score_{pos}_scale10"> '
                                for i in range(1,11):
                                    if rubric.get(i):
                                        out += f'<div><strong>{i}</strong> ➤ {rubric.get(i)}</div>'
                            else:
                                # Scale [1..5]
                                for i in range(0, 5):
                                    css_class = ""
                                    if i > 2:
                                        css_class += "sufficient"
                                    out += f'<label class="{css_class}">'
                                    out += f'<input required type="radio" {disabled}{"checked " if score==i else ""}name="score_{pos}" value="{i}"> '
                                    out += f'<strong>{i*25}%</strong>'
                                    if i in rubric:
                                        out += ' ➤ ' + self.render_text(rubric.get(i,""), alternatives, True)
                                    out += '</label>'
                        else: # An entry condition
                            must = rubric.get("must")
                            if isinstance(must, str):
                                out += f"<p>{must}</p>"
                            out += '<label>'
                            out += f'<input required type="radio" {disabled}{"checked " if score == 0 else ""}name="score_{pos}" value="no"> '
                            out += '<em>No.</em>'
                            out += '</label>'
                            out += '<label class="sufficient">'
                            out += f'<input required type="radio" {disabled}{"checked " if score == 1 else ""}name="score_{pos}" value="yes"> '
                            out += '<em>Yes.</em>'
                            out += '</label>'
                    else: # grading and not show_rubrics
                        color = "success"
                        if "range" in rubric:
                            content = f'{score*25}% ➤ {self.format_grade((rubric["range"][1]-rubric["range"][0])*score/4)} points'
                            if score < 3:
                                color = "danger"
                        else:
                            content = "✓" if score else "❌"
                            if not score:
                                color = "danger"
                        out += f'<div class="has-text-{color} has-text-centered is-primary mb-2">{content}</div>'
                    out += f'<textarea id="motivation_{pos}" class="textarea mt-1" placeholder="Motivation..." {disabled}name="motivation_{pos}">{flask.escape(motivation)}</textarea>'
                    out += '</div>'
                    if ai_feedback and (ai_obj := ai_feedback.get(f"Objective #{pos+1}")) and (ai_items := ai_obj.get("feedback")):
                        out += '<div class="ai-feedback notification is-info"><strong>AI feedback:</strong><ul>'
                        for line in ai_items:
                            out += f'<li onclick="addToMotivationBox(this)">{flask.escape(line)}</li>'
                        out += '</ul></div>'
                out += '</div>'
            elif "link" in objective:
                out += self.render_resource(objective)
            else: # it's a section
                title = f'<h{depth}>{self.render_text(objective["title"], alternatives, True)}</h{depth}>' if 'title' in objective else ''
                out += f'{title}{self.render_requirements(objective["children"], show_rubrics, grading, attempt, depth+1, ai_feedback)}'

        return out


    def render_grading(self, assignment, goals, show_rubrics, grading):
        if self.node.get('grading')==False:
            return ''

        # Filter out MUST rubrics
        all_rubrics = assignment['rubrics']
        rubrics = [rubric for rubric in all_rubrics if "range" in rubric]

        result = f'<h1>Rubrics mapping and grading</h1>'
        result += f'<table class="map-table"><thead><tr><th></th>'
        for idx, rubric in enumerate(all_rubrics):
            if "range" in rubric:
                result += f'<th>#{idx+1}</th>'
        result += f'<th>-</th><th>Σ</th></tr>\n</thead>'

        result += f'<tbody>'
        for goal_id, goal in goals.items():
            result += f'<tr><td>{goal["title"]}</td>'
            for rubric in rubrics:
                goal_range = rubric["goal_ranges"].get(goal_id)
                if goal_range:
                    result += f'<td>{self.format_grade(goal_range[1] - goal_range[0])}</td>'
                else:
                    result += '<td></td>'
            if goal_id in assignment["goal_ranges"]:
                result += f'<td>{self.format_grade(assignment["goal_ranges"][goal_id][0])}</td><th>{self.format_grade(assignment["goal_ranges"][goal_id][1])}</th></tr>\n'
            else:
                result += f'<td></td><th class="error">{self.format_grade(0)}</th></tr>\n'

        result += f'<tr><td><i>Base grade.</i></td>'
        for rubric in rubrics:
            result += f'<td></td>'
        result += f'<td>{self.format_grade(1)}</td><th>{self.format_grade(1)}</th></tr>\n'

        result += f'<tr class="highlight"><th>Σ</th>'
        grade = 1
        for rubric in rubrics:
            grade += rubric["range"][1]
            result += f'<th>{self.format_grade(rubric["range"][1] - rubric["range"][0])}</th>'
        result += f'<th>{self.format_grade(assignment["floor"])}</th><th>{self.format_grade(grade)}</th></tr>\n'

        if grading:
            scores = [grading.objective_scores[idx] if idx<len(grading.objective_scores) else 0 for idx in range(len(all_rubrics))]
        else:
            scores = [0]*len(all_rubrics) if show_rubrics==True else None

        all_weights = []
        if scores:
            result += f'<tr class="grading-scores"><th>Score</th>'
            for idx, rubric in enumerate(all_rubrics):
                if "range" in rubric:
                    result += f'<th>{round(scores[idx]*25)}%</th>'
            result += '<th>100%</th><th></th></tr>\n'

            result += f'<tr class="grading-grade highlight{" failed" if not grading or not grading.passed else ""}"><th>Grade</th>'
            grade = assignment["floor"]
            for idx, rubric in enumerate(all_rubrics):
                if "range" in rubric:
                    weight = rubric["range"][1] - rubric["range"][0]
                    score = scores[idx] / 4 * weight
                    grade += score
                    result += f'<th>{self.format_grade(score)}</th>'
                    all_weights.append(weight)
                else:
                    all_weights.append(None)
            result += f'<th>{self.format_grade(assignment["floor"])}</th><th>{self.format_grade(grade)}</th></tr>\n'

        result += '</tbody></table>'

        if scores and show_rubrics==True:
            result += f'<script src="/static/update-scores.js"></script><script>initUpdateScores(document.currentScript, {json.dumps(all_weights)}, {assignment["floor"]}, {self.passing_grade})</script>'

        return result

    def format_grade(self, grade):
        if -0.001 < grade < 0.001:
            return "" # just a rounding error
        # One digit after the decimal point, rounded down (or actually towards zero).
        if grade > 0:
            grade -= 0.0499999
        else:
            grade += 0.0499999
        return '{0:.1f}'.format(grade)

    def get_weights(self):
        return [(rubric["range"][1] - rubric["range"][0]) if "range" in rubric else None for rubric in self.assignment['rubrics']]

    def get_floor(self):
        return self.assignment["floor"]

    def form_to_scores(self, form: dict):
        scores = []
        for idx, rubric in enumerate(self.assignment['rubrics']):
            if "range" in rubric:
                value = form.get(f"score_{idx}_scale10")
                if value is not None:
                    # Score is [1..10], but we store everything on a scale of [0..4]
                    scores.append((float(value)-1) / 9 * 4)
                else:
                    scores.append(int(form[f"score_{idx}"]))
            else:
                scores.append(1 if form[f"score_{idx}"]=="yes" else 0)
        return scores

    def form_to_motivations(self, form: dict):
        motivations = []
        for idx in range(len(self.assignment['rubrics'])):
            motivations.append(form[f"motivation_{idx}"])
        return motivations

    @staticmethod
    def render_resource(resource_data):
        # Check if the learning resource exists in the database
        learning_resource = LearningResource.query.filter_by(url=resource_data.get("link")).first()
        if learning_resource is None:
            # Add the new learning resource to the database
            learning_resource = LearningResource(
                url=resource_data.get("link"), likes_count=0, dislikes_count=0
            )
            db.session.add(learning_resource)
            db.session.commit()

        user_resource_feedback = None
        user_authenticated = current_user.is_authenticated
        if user_authenticated:
            user_resource_feedback = UserResourceFeedback.query.filter_by(
                resource_id=learning_resource.id, user_id=current_user.id
            ).first()

        return flask.render_template(
            "learning_resource.html",
            learning_resource=learning_resource,
            user_resource_feedback=user_resource_feedback,
            resource_data=resource_data,
            user_authenticated=user_authenticated,
        )

    def calculate_grade(self, scores):
        grade = self.get_floor()
        entry_conditions = True
        for score, weight in zip(scores, self.get_weights()):
            if weight==None:
                if not score:
                    entry_conditions = False
            else:
                grade += weight * score / 4

        # In case entry conditions have not passed, the grade must be at least 1 lower
        # than the passing grade.
        grade = round(grade + 0.0001) # Make sure .5 is rounded up
        grade = max(1, min(10 if entry_conditions else self.passing_grade - 1, grade))
        passed = bool(grade >= self.passing_grade)
        return grade, passed


def render_template(directory, suffix="/"):
    html = ''
    for name in sorted(os.listdir(directory+suffix)):
        if name.startswith('.') or name in {'build', 'out', 'node_modules', 'target'}:
            continue

        path = directory+suffix+name
        ext = name.split('.')[-1]
        if ext == 'lock':
            continue

        if ext in {'svg', 'png', 'jpg'}:
            data = "Image data..."
        else:
            try:
                with open(path) as file:
                    data = file.read()
                if ext in {'csv', 'txt'}:
                    lines = data.rstrip().split("\n")
                    if len(lines) > 50:
                        lines = lines[:25] + ["...", "..."]
                    data = "\n".join(lines)
                if ext != 'md':
                    data = f"```{ext}\n{data}\n```"
            except IsADirectoryError:
                html += render_template(directory, suffix+name+"/")
                continue
            except UnicodeDecodeError:
                data = "Binary data..."
        
        html += f"<h1>Template file: {suffix.lstrip('/')+name}</h1>\n"
        html += f'<div class="template-file">{markdown_to_html(data)}</div>'
    return html


def get_all_variants_info(node, is_inspector, show_template=False):
    assignments = {}
    variant = 1
    while node.get(f'assignment{variant}'):
        ao = Assignment(node[f"assignment{variant}"], node)

        buttons = []
        template_html = ""
        template_dir = f"{node['directory']}/template{variant}"
        if os.path.isdir(template_dir):
            buttons.append(f"""<a role="button" href="/curriculum/{node['id']}/{variant}/template.zip">Download template</a>""")
            if show_template:
                template_html = render_template(template_dir)

        assignments[f"Variant {variant}"] = {
            "buttons": buttons,
            "html": ao.render(is_inspector) + template_html,
        }

        variant += 1
    return assignments

