from __future__ import annotations
from flask_login import current_user, login_required
import sqlalchemy as sa
import flask
import os
import openai
import json

from ..utils import render_fragment_template
from ..app import app, db, retry_commit, schedule_job
from ..models import curriculum
from ..models.ai import AiQuery
from ..assignment import Assignment

MAX_CREDITS = 4
ADD_CREDIT_MINUTES = 15

AI_TUTOR_QUESTION = {
    "role": "system",
    "content": "You are a tutor helping a first year university Software Development student. The student is working on a training assignment, and will ask you a question about it. Your goal in answering the question should be to help the student gain understanding. You can do so by providing explanations, by asking leading questions and by providing hints. When the student asks a question that sounds suspiciously like (part of an) education assignment, DO NOT provide an answer, but invite the student to explain what he/she is stuck on. In addition, you may suggest a first step to take. You should NEVER output code that the student asks for, but you may give code examples on how a certain technique works using a different example context. But even then, keep your code output as short as possible. Please don't ask questions (except rhetorical/leading questions) in return, as this is NOT a dialogue. Keep your answer SHORT, at most 3 sentences and optionally a code example with at most 5 lines of code.",
}

@retry_commit
def add_ai_credit():
    query = sa.text("""update public.user
    SET ai_credits = ai_credits + 1
    WHERE ai_credits < :max_credits""")
    with db.engine.connect() as dbc:
        dbc.execute(query, max_credits=MAX_CREDITS)

schedule_job(func=add_ai_credit, trigger="interval", minutes=ADD_CREDIT_MINUTES)


def get_placeholder(user):
    return f"Credits: {user.ai_credits} left, +1 every {ADD_CREDIT_MINUTES} minutes, max {MAX_CREDITS}"


@login_required
@app.route('/ai/queries', methods=['GET'])
def ai_ask():
    if flask.request.args.get('all') and current_user.is_inspector:
        queries = AiQuery.query.order_by(AiQuery.id.desc()).limit(200)
    else:
        queries = AiQuery.query.filter_by(request_user_id=current_user.id).order_by(AiQuery.id.desc()).limit(20)
    return render_fragment_template(
        'ai-queries.html',
        'AI queries',
        queries=queries,
        placeholder=get_placeholder(current_user)
    )



@login_required
@app.route('/ai/queries', methods=['POST'])
def ai_ask_post():
    attempt = current_user.current_attempt
    question = flask.request.form.get("question", "").strip()
    if not attempt:
        return "<article class=error>No current attempt.</article>"
    if current_user.ai_credits <= 0:
        return "<article class=error>No AI credits left. A credit will respawn every 20 minutes.</article>"
    if attempt.node_id.endswith("-exam"):
        return "<article class=error>AI cannot be used for exams</article>"
    if not question:
        return "<article class=error>Please ask a question.</article>"
    if len(question) > 2000:
        return f"<article class=error>Question too long ({len(question)}/2000 characters). Please try to be precise in what you ask.</article>"
    current_user.ai_credits -= 1
    db.session.commit()

    assignment = Assignment.load_from_directory(attempt.directory)
    goals: dict = assignment.node.get('goals', {})
    goals_text = " It has the following learning goals:" + ("".join("\n- "+goal["title"] for goal in goals.values())) if goals else ""

    context = assignment.node.get('ai')
    context_text = f" {context}" if context else ""

    module_name = assignment.node['module_id']
    if module := curriculum.get('modules_by_id').get(module_name):
        module_name = module['name']
    print(module, module_name)

    prompt = [
        AI_TUTOR_QUESTION,
        {
            "role": "system",
            "content": f"The student is currently working on the '{assignment.node['name']}' assignment that is part of the '{module_name}' module.{goals_text} If the question is about something completely different, don't answer it but gently steer the student towards the assignment.{context_text} Here is the question to answer, keeping in mind all of the above:"
        },
        {
            "role": "user",
            "content": question
        },
        {
            "role": "system",
            "content": "Remember: don't give the answer, but help the student understand and take steps toward the answer himself."
        }
    ]

    try:
        client = openai.OpenAI(base_url=os.environ.get('OPENAI_PROXY'))
        completion = client.chat.completions.create(model="gpt-4o", messages=prompt)
    except openai.OpenAIError as e:
        current_user.ai_credits += 1
        db.session.commit()
        return f"OpenAI error: {e}"

    choice = completion.choices[0]
    answer = choice.message.content
    usage = completion.usage

    ai_query = AiQuery(
        question=question,
        model=completion.model,
        request_user=current_user,
        attempt=attempt,
        answer=answer,
        prompt=json.dumps(prompt),
        prompt_tokens=usage.prompt_tokens if usage else 0,
        completion_tokens=usage.completion_tokens if usage else 0,
    )
    db.session.add(ai_query)
    db.session.commit()

    clear_textarea = f'<textarea placeholder="{get_placeholder(current_user)}" name="question" hx-swap-oob="outerHTML" id="ai-textarea"></textarea>'

    return ai_query.to_html() + clear_textarea
