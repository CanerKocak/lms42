from ..app import db, app
from flask_login import login_required, current_user
from ..models.learning_resource import LearningResource, UserResourceFeedback
from flask import request, render_template, abort


@app.route("/feedback/learning-resource", methods=["POST"])
@login_required
def handle_learning_resource_feedback():
    resource_id = request.form.get("resource_id")
    feedback_type = request.form.get("feedback_type")

    if resource_id and feedback_type in ["like", "dislike"]:
        try:
            resource_id = int(resource_id)
        except ValueError:
            print("Invalid resource_id value")
            return abort(400)  # Bad request

        learning_resource = LearningResource.query.get(resource_id)
        if not learning_resource:
            print(f"Learning resource with id {resource_id} not found")
            return abort(404)  # Resource not found

        existing_user_feedback = UserResourceFeedback.query.filter_by(
            user_id=current_user.id, resource_id=resource_id
        ).first()
        if existing_user_feedback:
            if existing_user_feedback.feedback_type == feedback_type:
                # User is providing the same feedback, so remove their feedback
                db.session.delete(existing_user_feedback)
            else:
                # User is changing their feedback
                existing_user_feedback.feedback_type = feedback_type
        else:
            # User is providing feedback for the first time
            new_user_feedback = UserResourceFeedback(
                user_id=current_user.id,
                resource_id=resource_id,
                feedback_type=feedback_type,
            )
            db.session.add(new_user_feedback)

        db.session.commit()
        learning_resource.update_feedback_counts()  # Update learning resource feedback counts

        # Render the feedback section template with the updated learning resource data
        user_resource_feedback = UserResourceFeedback.query.filter_by(
            user_id=current_user.id, resource_id=learning_resource.id
        ).first()
        return render_template(
            "learning_resource_feedback_buttons.html",
            learning_resource=learning_resource,
            user_resource_feedback=user_resource_feedback,
        )
    print(
        f"Invalid request values: resource_id={resource_id}, feedback_type={feedback_type}"
    )
    return abort(400)
