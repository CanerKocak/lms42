import sys
from lms42.models.ai import AiGrading, examine_attempt
from .. import utils, working_days
from ..app import db, app, retry_commit, schedule_job
from ..assignment import Assignment, get_all_variants_info
from ..models import curriculum
from ..models.attempt import Attempt, get_notifications, FORMATIVE_ACTIONS
from ..models.user import User
from ..models.feedback import NodeFeedback
from flask_login import login_required, current_user
from flask_wtf.csrf import generate_csrf
from pathlib import Path
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import datetime
import flask
import json
import os
import random
import re
import shutil
import subprocess
import sqlalchemy
from . import discord


DEFAULT_GIT_IGNORE = """.venv
node_modules
build
target
data
__pycache__
"""

GITLAB_URL = "https://gitlab.com/saxion.nl/42/lms42/-/issues?sort=created_date&state=all&label_name[]=feedback"

LMS_DIR = os.getcwd()

DEADLINE_MULTIPLIER = 1.5



# Work-around from: https://github.com/wtforms/wtforms/issues/477#issuecomment-716417410
# Should no longer be required after the next release of WTForms (after 2.3.3).
class FieldsRequiredForm(flask_wtf.FlaskForm):
    """Require all fields to have content. This works around the bug that WTForms radio
    fields don't honor the `DataRequired` or `InputRequired` validators.
    """

    class Meta:
        def render_field(self, field, render_kw):
            if field.type == "_Option" and field.name == "finished":
                render_kw.setdefault("required", True)
            return super().render_field(field, render_kw)



class SubmitForm(FieldsRequiredForm):
    finished = wtf.RadioField("Do you consider your submission finished?", choices=[
        ('yes', "Yes, I think it should be good enough"),
        ('forfeit', "No, but I'm giving up (for now)"),
    ], validators=[wtv.InputRequired()], description="If you answered no, feel free to skip the questions below.")

    resource_quality = wtf.RadioField('What did you think about the provided learning resources?', choices=[
        (1, 'Terrible'),
        (2, 'Not so good'),
        (3, 'Just okay'),
        (4, 'Pretty good'),
        (5, 'Excellent'),
    ])

    assignment_clarity = wtf.RadioField("Was it clear what the assignment wanted you to do?", choices=[
        (1, 'Not at all'),
        (2, 'No'),
        (3, 'A bit vague'),
        (4, 'Mostly clear'),
        (5, 'Perfectly clear'),
    ])

    difficulty = wtf.RadioField("How difficult did you find the assignment?", choices=[
        (1, 'Way too easy'),
        (2, 'Too easy'),
        (3, 'Just right'),
        (4, 'Too hard'),
        (5, 'Way too hard'),
    ])

    hours = wtf.IntegerField("How many (effective) hours did you spent on the assignment and learning resources?", validators=[wtv.NumberRange(0, 200)])

    fun = wtf.RadioField("Did you enjoy working on the assignment?", choices=[
        (1, 'Hated every moment'),
        (2, 'No'),
        (3, 'It was okay'),
        (4, 'For the most part'),
        (5, 'It was fun!'),
    ])

    comments = wtf.TextAreaField(app.jinja_env.filters['safe'](
        f"Any hints to help us improve? Anything you type here will be posted as a public (but anonymous to anyone but the teachers) <a href='{flask.escape(GITLAB_URL)}' target='_blank'>GitLab issue</a>."
    ), render_kw={"placeholder": "Your comment will be saved here until you submit the assignment."})

    submit = wtf.SubmitField('Submit attempt')



class EmptySubmitForm(FieldsRequiredForm):
    submit = wtf.SubmitField('Submit attempt')



@app.route('/curriculum', methods=['GET'])
def curriculum_get():
    student = get_student()
    periods = curriculum.get_periods_with_status(student)

    # Find the appropriate period tab to start with
    initial_block = "1.1"
    for block_name, block in periods.items():
        if any(topic.get("status") == "in_progress" for topic in block):
            initial_block = re.sub('[a-z]', '', block_name)
            break

    merged_periods = {}
    for block_name, period in periods.items():
        period_name = block_name.split('-')[0]
        if period_name not in merged_periods:
            merged_periods[period_name] = []
        merged_periods[period_name].append(period)

    if flask.request.cookies.get("layout") == "scroll":
        template = "curriculum_scroll.html"
    else:
        template = "curriculum.html"
    
    return flask.render_template(template,
        student=student,
        merged_periods=merged_periods,
        nodes_by_id=curriculum.get('nodes_by_id'),
        modules_by_id=curriculum.get('modules_by_id'),
        initial_block=initial_block,
        errors=curriculum.get('errors'),
        warnings=curriculum.get('warnings'),
        stats=curriculum.get("stats") if current_user.is_authenticated and current_user.is_inspector else None,
    )


@app.route('/attempts/<attempt_id>/handle_approval', methods=['POST'])
@utils.role_required('teacher')
def approve_attempt(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    if attempt.status == "awaiting_approval":
        if "approve" in flask.request.form:
            attempt.status = "in_progress"
            db.session.commit()
        else:
            # Somehow, this doesn't work in the unit test (non-deterministic FK error), probably
            # due to pytest-flask-sqlalchemy rollback magic.
            if attempt.student.current_attempt_id == attempt.id:
                attempt.student.current_attempt_id = None
                resume_paused_attempt(attempt.student)
                db.session.commit()
            delete_attempt(attempt)
    else:
        flask.flash("Approval was not needed.")
    return flask.redirect("/inbox")



@app.route('/attempts/<attempt_id>/consent', methods=['POST'])
@utils.role_required('teacher')
def consent_grading(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    grading = attempt.latest_grading

    if attempt.status != "needs_consent":
        flask.flash("No consent is required.")

    elif grading.grader_id == current_user.id:
        flask.flash("You cannot give consent to a grade you determined yourself.")
    
    else:
        grading.needs_consent = False
        grading.consent_user_id = current_user.id
        grading.consent_time = datetime.datetime.now(tz=datetime.timezone.utc)

        attempt.status = "passed" if grading.passed else "failed"
        db.session.commit()

        if attempt.credits > 0:
            grading.announce()


    return flask.redirect("/inbox")


@app.route('/mark_objective/', methods=["PUT"])
@login_required
@retry_commit
def mark_objective():
    if not current_user.is_student and not current_user.current_attempt:
        return {"error": "Cannot modify objective mark"}

    attempt = current_user.current_attempt

    data = json.loads(flask.request.data)

    objective_id = data['id']
    checked = data['checked']

    if checked:
        attempt.done_objectives.append(objective_id)
    else:
        attempt.done_objectives.remove(objective_id)

    db.session.commit()

    return {"success": True}

@app.route('/curriculum/<node_id>', methods=['POST'])
@login_required
@retry_commit
def node_action(node_id):
    if "start" in flask.request.form:
        return start_node(node_id)
    if "force-submit" in flask.request.form: # Needs to come before "submit" check
        return submit_node(node_id, force=True)
    if "submit" in flask.request.form:
        return submit_node(node_id)
    if "retract" in flask.request.form:
        return retract_approval_request(node_id)
    if "login" in flask.request.form:
        return flask.redirect("/user/login")
    raise Exception(f"Invalid action {flask.request.form}")


def retract_approval_request(node_id):
    attempt = current_user.current_attempt
    if attempt and attempt.node_id == node_id and attempt.status == 'awaiting_approval':
        current_user.current_attempt_id = None
        resume_paused_attempt(current_user)
        db.session.commit()
        delete_attempt(attempt)
    else:
        flask.flash("Nothing to retract.")
    return flask.redirect('#')


def delete_attempt(attempt):
    assert attempt.status in {"awaiting_approval", "awaiting_recording"}
    directory = attempt.directory
    db.session.delete(attempt)
    db.session.commit()
    shutil.rmtree(directory)


def start_node(node_id):
    node = curriculum.get_node_with_status(node_id, current_user)
    assert node
    errors, approvals, _, action = check_node_startable(node, current_user)
    if action != 'start':
        flask.flash(" ".join(errors))
        return flask.redirect(flask.request.url)
    
    last_number = db.session.query(Attempt.number) \
        .filter_by(student_id=current_user.id, node_id=node["id"]) \
        .order_by(Attempt.number.desc()).limit(1).scalar() or 0

    deadline = None
    if 'ects' in node and not node.get('allow_longer'):
        # Some time on the day of the deadline, in local time:
        deadline = datetime.datetime.now() + datetime.timedelta(days=(node['days']-1))
        # 17:00 on the day of the deadline, in local time:
        deadline = deadline.replace(**working_days.DEADLINE_TIME)
        # Convert to UTC
        deadline = utils.local_to_utc(deadline)

    # Select a semi-random variant giving preference to variants that the student has
    # attempted the least often.
    variant_occurrences = {}
    variant_id = 1
    while node.get(f'assignment{variant_id}'):
        # By default, take the first assignment first, unless it's an exam, than take a random assignment
        variant_occurrences[variant_id] = random.uniform(0, 1) if "ects" in node else variant_id/10
        variant_id += 1

    if not variant_occurrences:
        flask.flash("Sorry, there is no assignment available. Talk to a teacher!")
        return flask.redirect(flask.request.url)

    last_attempt = None
    attempts = Attempt.query.filter_by(student_id=current_user.id, node_id=node["id"]).order_by(Attempt.number)
    for attempt in attempts:
        last_attempt = attempt
        if attempt.status != "repair" and attempt.variant_id in variant_occurrences:
            variant_occurrences[attempt.variant_id] += 1

    variant_id = sorted(variant_occurrences.items(), key=lambda x: x[1])[0][0]
    assignment = node.get(f'assignment{variant_id}')

    alternatives = Assignment(assignment, node).get_alternatives()
    last_alternatives = last_attempt and last_attempt.status == "repair" and last_attempt.alternatives
    for name, options in alternatives.items():
        if last_alternatives and name in last_alternatives and last_alternatives[name] in options:
            # Try to preserve alternatives from the previous attempt to be repaired
            alternatives[name] = last_alternatives[name]
        else:
            # Select a random alternative
            alternatives[name] = random.choice(options)

    done_objectives = []

    if last_attempt and last_attempt.latest_grading and last_attempt.status == "repair":
        def filter_objectives(index: int):
            score = last_attempt.latest_grading.objective_scores[index]
            motivation = last_attempt.latest_grading.objective_motivations[index]
            # remove those that the teacher graded with less than 3 points or with 3 points and a comment.
            return not ((score < 3) or (score == 3 and motivation))
        
        done_objectives = list(filter(filter_objectives, last_attempt.done_objectives))

    if not node.get('allow_ai') and node.get('ects'):
        attempt_status = "awaiting_recording"
    elif approvals:
        attempt_status = "awaiting_approval"
    else:
        attempt_status = "in_progress"

    attempt = Attempt(
        student_id=current_user.id,
        student=current_user,
        number=last_number + 1,
        node_id=node["id"],
        variant_id=variant_id,
        deadline_time=deadline,
        status=attempt_status,
        credits=node.get("ects", 0),
        avg_days=node["avg_attempts"] * node["days"],
        alternatives=alternatives,
        done_objectives=done_objectives
    )

    error = None
    for _ in range(100): # High number, for tests
        try:
            os.makedirs(attempt.directory)
            break
        except FileExistsError as _error:
            error = _error
            attempt.number += 1
    else:
        print(f"Could not create attempt directory {attempt.directory} ({error} / {os.getcwd()})")
        flask.flash("Error! Could not create attempt directory.")
        return flask.redirect(flask.request.url)

    pausing_attempt = current_user.current_attempt
    if pausing_attempt:
        pausing_attempt.status = "paused"
        pausing_attempt.total_hours = pausing_attempt.hours # must be done before setting submit_time (see hours @property)
        pausing_attempt.submit_time = datetime.datetime.utcnow()
        flask.flash("Attempt paused!")

    db.session.add(attempt)
    db.session.commit() # causes attempt.id to be available
    current_user.current_attempt_id = attempt.id
    db.session.commit()

    # Write the attempt as json
    attempt.write_json()

    # Write the assignment as json
    with open(attempt.directory+"/assignment.json", "w") as file:
        file.write(json.dumps(assignment, indent=4))

    # Strip out useles data from the node, and write it to json
    node_copy = node.copy()
    variant_id2 = 1
    while node_copy.get(f'assignment{variant_id2}'):
        node_copy.pop(f'assignment{variant_id2}')
        variant_id2 += 1
    for name in ['directory', 'attempts', 'status']:
        node_copy.pop(name)

    with open(attempt.directory+"/node.json", "w") as file:
        file.write(json.dumps(node_copy, indent=4))

    # Copy the solution if it exists
    sol_dir = f"{node['directory']}/solution{variant_id}"
    if Path(sol_dir).is_dir():
        shutil.copytree(sol_dir, attempt.directory+"/solution")

    # Copy the template if it exists
    tmpl_src = f"{node['directory']}/template{variant_id}"
    tmpl_dst = attempt.directory+"/template"
    if Path(tmpl_src).is_dir():
        shutil.copytree(tmpl_src, tmpl_dst)
    else:
        os.mkdir(tmpl_dst)

    # Create a default .gitignore file if it doesn't exist
    gitignore = f"{tmpl_dst}/.gitignore"
    if not Path(gitignore).is_file():
        with open(gitignore, "w") as file:
            file.write(DEFAULT_GIT_IGNORE)

    # Do a git init in the template directory
    subprocess.run(["git", "init", "-q"], cwd=tmpl_dst)
    subprocess.run(["git", "add", "."], cwd=tmpl_dst)
    subprocess.run(["git", "commit", "-q", "--author", "sd42 <info@sd42.nl>", "-m", "Teacher-provided template"],
        cwd=tmpl_dst,
        env={'GIT_COMMITTER_NAME': 'sd42', 'GIT_COMMITTER_EMAIL': 'info@sd42.nl'}
    )

    # Get all the people doing the same assignment as the user
    student = current_user
    others = User.query \
        .filter_by(is_active=True, is_hidden=False, class_name=student.class_name) \
        .join(Attempt, User.id == Attempt.student_id) \
        .filter(Attempt.node_id == attempt.node_id) \
        .filter(Attempt.status == 'in_progress') \
        .filter(User.id != attempt.student_id) \
        .all()

    # Send an dm to the user if its not an exam attempt, has other people also doing the same assignment,
    # Has an discord id linked.
    # And has the message notification setting enabled.
    if len(others) >= 1 and attempt.credits == 0:
        for other_student in others:
            if other_student.discord_id and other_student.discord_notification_same_exercise:
                content = f"{student.short_name} just started working on the same assignment as you."
                discord.send_dm(discord_id=other_student.discord_id, title="Assignment info", content=content)

    return flask.redirect('#new')



def submit_node(node_id, force=False):
    attempt = Attempt.query.filter_by(student_id=current_user.id, node_id=node_id, status="in_progress").first()
    if attempt:
        submit_form = SubmitForm()

        # If the assignment was never uploaded or >10 minutes ago, give a warning
        if (attempt.upload_time is None or attempt.upload_time + datetime.timedelta(minutes=10) < datetime.datetime.utcnow()) and not force:
            if attempt.upload_time is None:
                flask.flash("No upload yet. Continue submission?")
            else:
                flask.flash("Your last upload was pretty long ago. Continue submission?")
            return flask.render_template('confirm_submit.html', form=submit_form, node_id=node_id)

        # Update the attempt state
        attempt.status = "needs_grading"
        attempt.finished = submit_form['finished'].data
        attempt.total_hours = attempt.hours # must be done before setting submit_time (see hours @property)
        attempt.submit_time = datetime.datetime.utcnow()
                
        # Set record status to finished
        if attempt.record_status in {"in_progress", "error"}:
            attempt.record_status = "finished"

        if current_user.current_attempt_id == attempt.id:
            current_user.current_attempt_id = None

        # Store feedback
        feedback = NodeFeedback()
        submit_form.populate_obj(feedback)
        feedback.node_id = node_id
        feedback.student_id = current_user.id
        db.session.merge(feedback)

        # Resume attempt
        resume_paused_attempt(current_user)

        # Flush
        db.session.commit()
        attempt.write_json()

        if '_pytest' not in sys.modules:
            print(f"Starting job examine_attempt_{attempt.id}")
            schedule_job(examine_attempt, args=(attempt.id,), id=f"examine_attempt_{attempt.id}")

        flask.flash("Assignment submitted!")
    else:
        flask.flash("Assignment already submitted?")
    return flask.redirect('/curriculum')



def resume_paused_attempt(user):
    attempt = user.paused_attempt
    if not attempt or user.current_attempt_id:
        return

    user.current_attempt_id = attempt.id
    attempt.status = "in_progress"
    attempt.submit_time = None
    attempt.start_time = datetime.datetime.utcnow()



@app.route('/curriculum/<node_id>/<int:variant_id>/template.zip', methods=['GET'])
def node_get_template(node_id, variant_id):
    node = curriculum.get('nodes_by_id')[node_id]
    is_public = node.get('public_for_students') if current_user.is_authenticated else node.get('public_for_externals')
    if is_public or (current_user.is_authenticated and (current_user.is_inspector or not node.get('ects'))):
        zip_proc = subprocess.Popen(['zip', '-r', '-', f'template{variant_id}'], stdout=subprocess.PIPE, cwd=node["directory"])
        return flask.Response(zip_proc.stdout, content_type='application/zip')
    return 'Lesson is not public.', 403



@app.route('/curriculum/<node_id>', methods=['GET'])
def node_get(node_id):
    status_code = 200
    if flask.request.args.get("poll_approved") and current_user.current_attempt and current_user.current_attempt.node_id == node_id:
        if current_user.current_attempt.status == "awaiting_approval":
            return "Waiting..", 204 # HTMX interprets this status code as no data
        status_code = 286 # HTMX interprets this status code as 'stop polling'

    student = get_student()
    status = None
    node = curriculum.get_node_with_status(node_id, student)
    if not node:
        return flask.render_template('404.html', message='No such lesson'), 404

    pair_options = []
    assignments = {}

    if student:
        # Show all attempts for a specific student in tabs
        passed = False
        for attempt in reversed(node["attempts"]):
            regrade = int(flask.request.args.get("regrade", "-1")) == attempt.id
            assignments[f"Attempt {attempt.number}"] = get_attempt_info(node, attempt, student, regrade)
            if attempt.status == 'passed':
                passed = True

        if (passed and not node.get('ects')) or node.get("public_for_students"):
            variants = get_all_variants_info(node, current_user.is_authenticated and current_user.is_inspector)
            if len(variants) == 1 and assignments:
                assignments["Latest"] = list(variants.values())[0]
            else:
                assignments.update(variants)

        if node["status"] != "in_progress":
            errors, approvals, warnings, action = check_node_startable(node, student)
            if current_user.id != student.id:
                action = None

            if action == 'start':
                if approvals:
                    action_text = 'Request teacher approval'
                elif warnings:
                    action_text = 'Start attempt anyway'
                else:
                    action_text = 'Start attempt'
            elif action == 'retract':
                action_text = 'Retract approval request'
            else:
                action_text = None

            status = {'action': action, 'action_text': action_text, 'errors': errors, 'approvals': approvals, 'warnings': warnings}

        if node.get('pair') and not passed:
            # Look for students who have not finished this node, but *have* finished the node before (the node before).
            prev_node = curriculum.get_previous_node(node)
            if prev_node:
                TargetAttempt = sqlalchemy.orm.aliased(Attempt)  # noqa: N806
                # Select other active students from the same city
                pair_options = User.query.filter_by(is_student=True, is_active=True, is_hidden=False) \
                    .filter(User.id != student.id) \
                    .filter(User.class_name.like((student.class_name or 'x')[0] + '%'))

                PrevAttempt = sqlalchemy.orm.aliased(Attempt)  # noqa: N806
                prev_prev_node = curriculum.get_previous_node(prev_node)
                if prev_prev_node:
                    # Look for students who have finished the assignment before the previous assignment
                    pair_options = pair_options.join(PrevAttempt, sqlalchemy.and_(PrevAttempt.student_id == User.id, PrevAttempt.status == 'passed', PrevAttempt.node_id == prev_prev_node['id']))
                else:
                    # Look for students who have an attempt (in any state) for the previous assignment
                    pair_options = pair_options.join(PrevAttempt, sqlalchemy.and_(PrevAttempt.student_id == User.id, PrevAttempt.node_id == prev_node['id']))

                # Only select students that have not yet passed the target node
                pair_options = pair_options \
                    .outerjoin(TargetAttempt, sqlalchemy.and_(TargetAttempt.student_id == User.id, TargetAttempt.status != 'failed', TargetAttempt.node_id == node['id'])) \
                    .filter(TargetAttempt.student_id == None)  # noqa: E711 (sqlalchemy can't work with 'is')

    elif not student and ((current_user.is_authenticated and current_user.is_inspector) or node.get('public_for_externals')):
        # View assignment, not attached to a specific student
        assignments = get_all_variants_info(node, current_user.is_authenticated and current_user.is_inspector, bool(flask.request.args.get("template")))
    else:
        status = {'action': 'login', 'action_text': 'Login', 'errors': ["You'll need to login to view this assignment."]}

    return flask.render_template('node.html',
        topic=curriculum.get('modules_by_id').get(node.get("module_id")),
        student=student,
        node=node,
        pair_options=pair_options,
        assignments=assignments,
        status=status,
        base_url=f"/curriculum/{node['id']}/static/",
        outcomes_by_id=curriculum.get('outcomes_by_id'),
        endterms=curriculum.get('endterms'),
    ), status_code



RECORD_STATES = {"in_progress", "awaiting_approval", "awaiting_recording"}

def get_attempt_info(node, attempt, student, force_new_grading=False):
    """View assignment attempt for a specific student."""

    result: dict = {
        'html': '',
        'submit_warnings': [],
        'attempt_id': attempt.id,
    }

    if current_user == student:
        # Determine if this assignment currently requires recording
        if attempt.status in RECORD_STATES and node.get("ects") and not node.get("allow_ai"):
            result['require_recording'] = attempt.record_status or 'start'

        if attempt.status == 'awaiting_approval':
            result['html'] += f'''Awaiting teacher approval...<span hx-get="/curriculum/{node["id"]}?poll_approved=true" hx-trigger="every 5s" hx-target="body"></span>'''

        # While the student is waiting to start an exam, don't show anything yet
        if attempt.status in {"awaiting_approval", "awaiting_recording"}:
            return result
    
    needs_grading = (attempt.status == "needs_grading") or force_new_grading
    show_rubrics = (True if needs_grading else "disabled") if current_user.is_teacher else False
    
    # Load the grades, if any
    latest_grading = attempt.latest_grading
    if latest_grading is None and needs_grading and current_user.is_teacher:
        # Help the teacher by prefilling the grading from a previous attempt that needed to be repaired.
        prev_attempt = Attempt.query.filter(
            Attempt.student_id == attempt.student_id,
            Attempt.node_id == attempt.node_id,
            Attempt.variant_id == attempt.variant_id,
            Attempt.status != "needs_grading",
        ).order_by(Attempt.submit_time.desc()).first()

        if prev_attempt and prev_attempt.status == "repair":
            latest_grading = prev_attempt.latest_grading

    # Render the actual assignment to HTML
    if attempt.data_deleted:
        result['html'] = "The content for this attempt is no longer available."
    else:
        ao = Assignment.load_from_directory(attempt.directory)
        ai_grading = AiGrading.query.filter_by(attempt_id=attempt.id).order_by(AiGrading.id.desc()).first() if current_user.is_teacher else None

        ai_grading_feedback = None
        if current_user.is_teacher and ai_grading:
            try:
                ai_grading_feedback = json.loads(ai_grading.output)
            except:
                pass
        
        if not current_user.is_teacher and "ects" in node and attempt.status != "in_progress":
            result['html'] = "<div>Exams can only be viewed while taking them. If you'd like to review your exam and see how it was graded, ask a teacher.</div>"
            shown_grading = latest_grading if latest_grading and not latest_grading.needs_consent else None
            result['html'] += ao.render_grading(ao.assignment, ao.goals, show_rubrics, shown_grading)
        else:
            result['html'] = ao.render(show_rubrics, latest_grading, attempt, ai_grading_feedback)

    result['notifications'] = get_notifications(attempt, attempt.latest_grading)
    
    if current_user.is_teacher:
        if attempt.max_screenshot_id > 0:
            result['html'] += f'''
            <div class="buttons is-right mt-2" style="margin-top: 2.4rem;">
                <input class="button" type="button" value="Review screen recording" onclick='reviewRecording("{attempt.id}")'>
                <script src="/static/record-review.js"></script>
            </div>
            '''

        if needs_grading:
            
            result['html'] = f'''
                <form method="post" action="/inbox/grade/{attempt.id}" >
                    {result['html']}
                    <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                    <h1>Finalize grading</h1>
                    {"" if "ects" in node else render_formative_action(attempt.status)}
                    <textarea placeholder="Motivation..." class="textarea" name="motivation">{flask.escape(latest_grading.grade_motivation) if latest_grading and latest_grading.grade_motivation else ''}</textarea>
                    <div class="buttons is-right mt-2">
                        <script src="/static/fill-not-graded.js"></script>
                        <button type="button" class="outline" onclick="fillAllGood(this)">All good</button>
                        <button type="button" class="outline" onclick="fillNotGraded(this)">Not graded</button>
                        {'<input class="button is-warning" type="submit" name="request_consent" value="Request consent">' if "ects" in node else ''}
                        <input class="button is-primary" type="submit" name="publish" value="Publish">
                    </div>
                </form>
            '''

            others = User.query \
                .filter_by(is_active=True, is_hidden=False) \
                .join(Attempt, User.id == Attempt.student_id) \
                .filter_by(node_id=attempt.node_id, variant_id=attempt.variant_id, status='needs_grading') \
                .filter(User.id != attempt.student_id)

            result['others'] = [(other, f"/curriculum/{attempt.node_id}?student={other.short_name}") for other in others]
            result['others_message'] = "Other node attempts in need of grading:"
        else:
            # The form is required to keep the name spaces for radio buttons separate when we're showing multiple attempts
            result['html'] = f'<form>{result["html"]}</form>'

            if attempt.status == "awaiting_approval":
                _errors, approvals, _warnings, _action = check_node_startable(node, student)
                approvals = "".join(f"<li>{approval}</li>" for approval in approvals)
                result['notifications'].append(f'''
                    Start of this assignment needs to be approved.
                    <ul>{approvals}</ul>
                    <form method="post" action="/attempts/{attempt.id}/handle_approval" class="buttons is-right mt-2 is-primary">
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        <input role="button" class="outline" type="submit" name="decline" value="Decline">
                        <input role="button" type="submit" name="approve" value="Approve">
                    </form>
                ''')
            elif attempt.status in {"needs_consent", "passed", "repair", "failed"}:
                if attempt.status == "needs_consent":
                    consent = '<input class="button is-primary" type="submit" name="consent" value="Consent grade">'
                else:
                    consent = ''
                if attempt.data_deleted:
                    regrade = ''
                else:
                    regrade = f"""<a class="button" href="/curriculum/{node['id']}?student={attempt.student_id}&amp;regrade={attempt.id}">Regrade</a>"""
                result['html'] += f'''
                    <form method="post" action="/attempts/{attempt.id}/consent" class="buttons is-right mt-2 is-primary">
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        {regrade}
                        {consent}
                    </form>
                '''

    # It's the actual student. Show submit action in case this is the current attempt.
    if current_user == student and attempt.status == 'in_progress':
        assert attempt == student.current_attempt

        # Create the submit form
        old_values = NodeFeedback.query.filter_by(node_id=attempt.node_id, student_id=student.id).first()
        result['submit_form'] = SubmitForm(obj=old_values) if node.get('feedback', True) else EmptySubmitForm()
        if result['submit_warnings']:
            result['submit_form'].submit.label.text += " ignoring warnings"
            result['submit_form'].submit.render_kw = {'class_': 'button is-warning'}

        # Suggest classmates to seek out for help
        if not node.get("ects"):
            others = User.query \
                .filter_by(is_active=True, is_hidden=False, class_name=student.class_name) \
                .join(Attempt, User.id == Attempt.student_id) \
                .filter(Attempt.node_id == attempt.node_id) \
                .filter(Attempt.student_id != attempt.student_id) \
                .order_by(Attempt.start_time.desc()) \
                .limit(5)
            
            result['others'] = [(other, f"/people/{other.short_name}") for other in others]
            result['others_message'] = "Most recent classmates to work on this assignment:"

    return result



def render_formative_action(status):
    options = [f'<option value="{k}"{" selected" if k == status else ""}>{v}</option>' for k, v in FORMATIVE_ACTIONS.items()]
    return f'''
        <div class="select mb-2 mt-2">
            <select name="formative_action" required>
                <option value="">What should happen?</option>
                {"".join(options)}
            </select>
        </div>
    '''



def check_node_startable(node, student):
    action = 'start'
    warnings = []
    approvals = []
    errors = []
    
    warn_exam = False
    status = node["status"]
    if status == "startable":
        pass
    elif status == "wip":
        approvals.append("Sorry, this module is not ready yet. Please talk to your teachers!")
    elif status == "paused":
        errors.append("This assignment is on pause. Finish your current assignment to resume.")
    elif status == "in_progress":
        action = None
    elif status == "future":
        if not node.get("ignore_previous_nodes", False):
            approvals.append("At least one previous node hasn't been completed yet.")
    elif status == "almost_startable" and not node.get("ignore_previous_nodes", False):
        warnings.append("The previous node hasn't been graded yet.")
    elif status == "needs_grading" or status == "needs_consent":
        warnings.append("You have already submitted this assignment, but it hasn't been graded yet.")
    elif status == "passed":
        warnings.append("There's no need to start this assignment again.")
    elif status == "awaiting_recording":
        warn_exam = True
    else:
        errors.append(f"Unknown status: {status}.")

    if node.get("ects") and node.get("exam_approval", True):
        approvals.append("Teacher approval is required to start the exam.")
        warn_exam = True

    if warn_exam:
        warnings.append("Please review the <a href='/coc#exams' target='_blank'>exam CoC</a> before you start.")
        if not node.get("allow_ai"):
            warnings.append("In particular: remember to disable and refrain from using AI tools!")
        warnings.append("Close all windows and tabs unrelated to the exam, in order to protect your privacy and stay within the CoC exam rules.")

    if node.get("start_when_ready"):
        warnings.append("As you'll want to interleave other modules with your work on this assignment, you should delay starting the attempt until you're ready to submit your work.")

    if node.get('wip'):
        approvals.append("Sorry, this lesson is not ready yet. Please talk to your teachers!")

    current_attempt = student.current_attempt
    if current_attempt:
        if current_attempt.node_id == node['id']:
            if current_attempt.status == "awaiting_approval":
                errors.append("Go find a teacher to approve the assignment!")
                action = 'retract'
            elif current_attempt.status == "awaiting_recording":
                errors.append("Start screen recording or find a teacher in case of a problem.")
        elif current_attempt.credits > 0:
            errors.append("You cannot start another assignment while you are working on an exam.")
        elif student.paused_attempt:
            errors.append("You are already working on another assignment and you already have a paused attempt.")
        else:
            warnings.append("You are already working on another assignment. Starting anyway will pause your current attempt.")

    if node.get("in_progress_topics", 0) >= 3 and node["node_index"] == 0:
        approvals.append(f"You're already working on {node['in_progress_topics']} modules.")

    if action == 'start' and errors:
        action = None

    return errors, approvals, warnings, action



@app.route('/curriculum/<node_id>/static/<name>', methods=['GET'])
def node_file(node_id, name):
    node = curriculum.get('nodes_by_id')[node_id]
    if not node.get('public_for_externals') and not current_user.is_authenticated:
        return "Permission denied", 403
    static_dir = f"{LMS_DIR}/{node['directory']}/static"
    return flask.send_from_directory(static_dir, name)



def get_student():
    if current_user.is_authenticated:
        student_id = flask.request.args.get('student')
        if student_id and current_user.is_teacher:
            if student_id.isdigit():
                return User.query.get(student_id)
            return User.query.filter_by(short_name=student_id).first()
        if current_user.is_authenticated and not current_user.is_inspector:
            return current_user
    return None
