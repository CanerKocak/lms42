from ..app import db
import sqlalchemy as sa


class LearningResource(db.Model):
    __tablename__ = "learning_resources"
    id = sa.Column(sa.Integer, primary_key=True)
    url = sa.Column(sa.String, nullable=False, unique=True)
    likes_count = sa.Column(sa.Integer, nullable=False, default=0)
    dislikes_count = sa.Column(sa.Integer, nullable=False, default=0)

    def update_feedback_counts(self):
        """Update the learning resource's likes and dislikes counts."""
        likes_count = UserResourceFeedback.query.filter_by(
            resource_id=self.id, feedback_type="like"
        ).count()
        dislikes_count = UserResourceFeedback.query.filter_by(
            resource_id=self.id, feedback_type="dislike"
        ).count()
        self.likes_count = likes_count
        self.dislikes_count = dislikes_count
        db.session.commit()


class UserResourceFeedback(db.Model):
    __tablename__ = "user_resource_feedbacks"
    resource_id = sa.Column(sa.Integer, sa.ForeignKey("learning_resources.id"), primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.id"), primary_key=True)
    feedback_type = sa.Column(sa.Enum("like", "dislike", name="feedback_type_enum"), nullable=False)

    learning_resource = sa.orm.relationship("LearningResource",
        backref=sa.orm.backref("user_feedbacks", cascade="all, delete-orphan"),
    )
    user = sa.orm.relationship("User",
        backref=sa.orm.backref("resource_feedbacks", cascade="all, delete-orphan"),
    )
