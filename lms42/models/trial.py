from ..app import db, get_base_url
import datetime
import sqlalchemy as sa
from ..utils import generate_password, format_date
from ..email import send as send_email


class TrialDay(db.Model):
    date = sa.Column(sa.Date, primary_key=True)

    slots = sa.Column(sa.Integer, nullable=False, default=2)


class TrialStudent(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    secret = sa.Column(sa.String, nullable=False, default=generate_password)

    first_name = sa.Column(sa.String, nullable=False)
    last_name = sa.Column(sa.String, nullable=False)
    email = sa.Column(sa.String, nullable=False, unique=True)

    invitation_time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    trial_day_date = sa.Column(sa.Date, sa.ForeignKey('trial_day.date'), nullable=True)
    trial_day = sa.orm.relationship("TrialDay", backref="students")

    student_id = sa.Column(sa.Integer)
    submit_date = sa.Column(sa.Date)
    modify_date = sa.Column(sa.Date)
    city = sa.Column(sa.String)
    enrollment_date = sa.Column(sa.Date)
    enrollment_state = sa.Column(sa.String)
    admission_state = sa.Column(sa.String)
    nationality = sa.Column(sa.String)
    correspondence_city = sa.Column(sa.String)
    correspondence_country = sa.Column(sa.String)
    language = sa.Column(sa.String)
    prior_education = sa.Column(sa.String)
    phone = sa.Column(sa.String)

    comment = sa.Column(sa.String)
    manual_probability = sa.Column(sa.Integer)
    did_online_intake = sa.Column(sa.Boolean, server_default='false')


    @property
    def url(self):
        return f"{get_base_url()}/trial/{self.id}/{self.secret}"


    @property
    def probability(self):
        if self.manual_probability != None:
            return self.manual_probability
        if self.enrollment_state == 'retracted':
            return 0
        if not self.enrollment_state: # Not enrolled yet (but registered for trial on open day)
            if self.trial_day_date:
                return 70
            else:
                return 40
        if self.language == 'Nederlands' or self.correspondence_country == 'Nederland' or self.nationality == 'Nederland':
            if self.trial_day_date:
                return 85
            else:
                return 60
        else:
            if self.trial_day_date:
                return 40
            elif self.admission_state == 'Toegelaten':
                return 10
            else:
                return 2


    def send_invite_email(self):
        send_email(self.email, "Saxion invitation", f"""Hi {self.first_name},

You are warmly invited to participate in a trial day for the Associate degree Software Development at Saxion. Please refer to this link for more information and to schedule your visit:

{self.url}

If you are unable to attend a trial day due to other commitments or because you are located abroad, please don't hesitate to contact me to arrange an online intake or to discuss alternative dates.

To ensure that you've made the right choice while you still have other options, we recommend that you make an appointment as soon as possible. We're looking forward to your visit!

Best regards,

Associate degree Software Development
Saxion University of Applied Sciences

Any questions? Feel free to reply to this message!
""")



    def send_scheduled_email(self):
        send_email(self.email, "Trial day scheduled", f"""Hi {self.first_name},
                
You scheduled your trial day for the Saxion Ad Software Development on {format_date(self.trial_day_date)}.

Instructions for your visit:
- We're located at Van Galenstraat 19, Enschede. Take the elevators to the 5th floor, and turn left into the long corridor. Our offices are at the end, in room G5.27.
- Please try to arrive between 8:45 and 9:00 in the morning. The day ends around 16:00, although you're of course free to leave at any time.
- Bring your laptop and headphones.
- You may want to bring lunch, or you can buy some in the canteen.
- In case you need to cancel/reschedule, please do so by visiting the scheduling page again: {self.url}

We're looking forward to your visit!

Best regards,

Associate degree Software Development
Saxion University of Applied Sciences

Any questions? Feel free to reply to this message!
""")


    def send_cancel_email(self, date):
        send_email(self.email, "Trial day cancelled", f"""Hi {self.first_name},
    
Your Saxion Ad Software Development trial day appointment on {format_date(date)} has been cancelled.

Best regards,

Associate degree Software Development
Saxion University of Applied Sciences

Any questions? Feel free to reply to this message!
""")
