#!/usr/bin/env python3

from configparser import ConfigParser
from pathlib import Path
from glob import glob
import argparse
import json
import os
import pathlib
import secrets
import socket
import sys
import urllib.request
import subprocess
import shlex
from sys import platform

import shutil

BASE_URL = os.environ.get('LMS_BASE_URL') or 'https://sd42.nl'
CLI_VERSION = 17
FALLBACK = ["code .", "vscode .", "codium ."]  # The default IDEs for opening a template


def run_ide(out_dir):
    """Helper function for running ides in the shell.
    
    This function checks if the template contains a `.lms-ide` file (containing ide commands) and if so, reads it.
    Any commands found will be put into a list; when no commands are found an empty list will be created.
    This list will be extended with the default ide commands as specified in `FALLBACK`.
    Note that the commands in the file wil take precedence over the fallback commands.

    This function then tries to execute the commands by order of index and will keep trying until exit-code 0 is
    returned by the application. If no command is successfully executed, an error message will be printed.

    Args:
     out_dir (string): The directory to be appended to the command.

    Returns:
     never
    """

    os.chdir(out_dir)

    try:
        with open(".lms-ide") as file:
            commands = file.read().splitlines()
    except FileNotFoundError:
        commands = []
    
    commands += FALLBACK

    for command in commands:
        command = shlex.split(command)
        try:
            os.execvp(command[0], command)
        except:
            pass

    print("Error: Could not execute any of", commands)
    sys.exit(1)


class LmsCli:
    def __init__(self, args):
        self.__up_to_date = False

        parser = argparse.ArgumentParser("lms")
        parser.add_argument('-v', '--verbose', default=0, action='count', help="enable verbose output")
        subparsers = parser.add_subparsers(title="command", dest="command", required=True)
        
        install_parser = subparsers.add_parser("install", help="install or upgrade lms")
        install_parser.add_argument('--finish', action="store_true", help=argparse.SUPPRESS)

        login_parser = subparsers.add_parser("login", help="connect to your sd42.nl account")

        template_parser = subparsers.add_parser("template", help="download the current assignment template")

        upload_parser = subparsers.add_parser("upload", help="upload your work for the current assignment")

        download_parser = subparsers.add_parser("download", help="download your last-submitted work for the current assignment")
        download_parser.add_argument("spec", help="the node id optionally followed by a '~' and the attempt number, eg vars~2")

        verify_parser = subparsers.add_parser("verify", help="verify the integrity of your lms directory")

        grade_parser = subparsers.add_parser("grade", help="teachers only: download everything needed for grading a student attempt")
        grade_parser.add_argument("spec", help="the student's short name optionally followed by '@' the node id and '~' attempt number, eg frank@vars~2")

        open_parser = subparsers.add_parser("open", help="open the current assignment in your IDE")

        self.opts = parser.parse_args(args)
        
        if self.opts.verbose >= 2:
            print("Arguments:", self.opts)
        
        self.config_path = os.path.join(os.environ['HOME'], '.config', 'lms.ini')
        self.config = ConfigParser()
        if pathlib.Path(self.config_path).is_file():
            self.config.read(self.config_path)

        for section in ('auth', 'setup',):
            if section not in self.config:
                self.config.add_section(section)

        if self.opts.verbose >= 2:
            print("Config:", {section: dict(self.config[section]) for section in self.config.sections()})

        getattr(self, "handle_"+self.opts.command.replace('-','_'))()


    def handle_install(self):
        if self.opts.finish:
            # This is the new version being called immediately post install
            print(f"Successfully installed version {CLI_VERSION} to {__file__}")
            if self.config.getboolean("setup", "enabled", fallback=True):
                setup_computer()
        else:
            path = self.update()
            os.execv(path, ["lms", "install", "--finish"])


    @staticmethod
    def update():
        bin_dir = os.path.join(os.environ['HOME'], '.local', 'bin')
        lms_path = os.path.join(bin_dir, "lms")
        pathlib.Path(bin_dir).mkdir(parents=True, exist_ok=True)

        with urllib.request.urlopen(BASE_URL+'/static/lms') as response:
            file = os.open(lms_path, os.O_CREAT | os.O_WRONLY | os.O_TRUNC, 0o755)
            os.write(file, response.read())
            os.close(file)

        return lms_path


    def handle_login(self):
        # Generate a random token and save it to our config file
        token = secrets.token_urlsafe()
        self.config.set('auth', 'token', token)
        with open(self.config_path, 'w') as file:
            self.config.write(file)
        
        # Ask the user to authorize the token
        host = socket.gethostname()
        url = f"{BASE_URL}/api/authorize?host={urllib.parse.quote(host, safe='')}&token={token}"
        print(f"Go to this URL to authorize lms: {url}")
        os.system(f"xdg-open '{url}' 2> /dev/null")


    def request(self, path, data=None, return_response=False):
        # Combine BASE_URL, path and the API version number to a URL
        url = f"{BASE_URL}{path}{'&' if '?' in path else '?'}v={CLI_VERSION}"
        req = urllib.request.Request(url)
        req.add_header('authorization', self.config.get("auth", "token"))

        try:
            response = urllib.request.urlopen(req, data)
        except urllib.error.HTTPError as e:
            if e.code == 418: # I'm a teapot: our client is outdated
                # Upgrade the client
                self.update()
                print(f"Auto-upgraded lms client")
                # And call the new version with the same arguments
                os.execvp(sys.executable, [sys.executable] + sys.argv)
                self.die("Client upgrade failed")

            self.die(f"HTTP response code {e.code} requesting {path}:\n{e.read().decode()}")

        if not self.__up_to_date:
            self.__up_to_date = True
            if self.config.getboolean("setup", "enabled", fallback=True):
                setup_computer()

        if return_response:
            return response

        content = json.loads(response.read())
        response.close()
        if isinstance(content,dict) and "error" in content:
            self.die(content.get("message") or content.get("error"))
        return content


    def get_current_attempt(self):
        attempt = self.request("/api/attempts/current")
        if not attempt:
            self.die("no current assignment")
        return attempt


    @staticmethod
    def get_attempt_dir(attempt):
        return os.path.join(os.environ['HOME'], 'lms', attempt['path'])


    def handle_template(self):
        if self.config.getboolean("setup", "move_node_directories", fallback=True):
            self.move_node_directories()

        attempt = self.get_current_attempt()
        out_dir = self.get_attempt_dir(attempt)

        if not self.download_template(attempt, out_dir):
            self.die(f"output directory {out_dir} already exists. Delete it first if you want to start with a fresh template.")


    def download_template(self, attempt, out_dir):
        try:
            os.makedirs(out_dir)
        except OSError:
            return False

        self.download_tgz(f"/api/attempts/{attempt['attempt_id']}/template", out_dir)

        print(f"Created {out_dir}")
        return True


    def download_tgz(self, path, out_dir):
        """Download and extract a gzipped tar archive from `path` and extract it into
        the `out_dir` directory, which must already exist."""
        response = self.request(path, return_response=True)

        tar = subprocess.Popen(['tar', 'xzC', out_dir], stdin=subprocess.PIPE)
    
        while True:
            chunk = response.read(64*1024)
            if not chunk:
                break
            tar.stdin.write(chunk)
        tar.stdin.close()
        tar.wait()


    def handle_download(self):
        attempts = self.request(f"/api/attempts/@{self.opts.spec.replace('~',':')}")
        if not attempts:
            self.die("no attempt found")
        attempt = attempts[0]
        out_dir = self.get_attempt_dir(attempt)

        try:
            os.makedirs(out_dir)
        except OSError:
            self.die(f"output directory {out_dir} already exists. Delete it first if you want to download your submitted work.")

        self.download_tgz(f"/api/attempts/{attempt['spec']}/submission", out_dir)
        print(f"Created {out_dir}")


    def handle_grade(self):
        attempts = self.request(f"/api/attempts/{self.opts.spec.replace('~',':')}")
        if not attempts:
            self.die("no attempt found")
        attempt = attempts[0]

        out_dir = os.path.join(os.environ['HOME'], 'lms', 'grading', attempt['spec'].replace(':','~'))

        # Delete the target directory if it is empty
        try:
            os.unlink(out_dir)
        except OSError:
            pass

        # Download the solution to the target direction, if it doesn't exist
        try:
            os.makedirs(out_dir)
        except OSError:
            print(f"Submission already exists in {out_dir}")
        else:
            self.download_tgz(f"/api/attempts/{attempt['spec']}/submission", out_dir)
            print(f"Downloaded to {out_dir}")

        # Symlink our local solution and node directories (if any)
        for name in ["_node", "_solution", "_template"]:
            try:
                os.unlink(out_dir+"/"+name)
            except OSError:
                pass
        curriculum_dir = os.path.expanduser(self.config.get("grade", "lms_directory", fallback="~/lms42"))
        node_dir = f"{curriculum_dir}/assignments/{attempt['node_id']}"
        if os.path.isdir(node_dir):
            os.symlink(node_dir, out_dir+"/_node")
            for what in ["solution", "template"]:
                what_dir = f"{node_dir}/{what}{attempt['variant_id']}"
                if os.path.isdir(what_dir):
                    os.symlink(what_dir, f"{out_dir}/_{what}")

        run_ide(out_dir)


    def handle_upload(self):
        if self.config.getboolean("setup", "move_node_directories", fallback=True):
            self.move_node_directories()
        
        attempt = self.get_current_attempt()
        out_dir = self.get_attempt_dir(attempt)

        if not Path(out_dir).is_dir():
            self.die(f"assignment directory {out_dir} does not exist")

        cmd = 'gtar' if platform == 'darwin' else 'tar'
        try:
            tar = subprocess.Popen([cmd, 'czC', out_dir, '--exclude-backups', '--exclude-ignore=.gitignore', '--exclude-ignore=.lmsignore', '.'], stdout=subprocess.PIPE)
        except FileNotFoundError:
            print(f"Command not found: '{cmd}'.")
            if platform == 'darwin':
                print("Please install gnu-tar (using brew for instance).")
            sys.exit(1)
    
        result = self.request(f"/api/attempts/{attempt['attempt_id']}/submission", data=tar.stdout)
        tar.wait()
        
        print(f"Upload complete: {result['transferred']//1024}kb transferred")
        print("Please remember that you still need to submit in the web interface")


    def handle_open(self):
        if self.config.getboolean("setup", "move_node_directories", fallback=True):
            self.move_node_directories()

        attempt = self.get_current_attempt()
        out_dir = self.get_attempt_dir(attempt)
        if not self.download_template(attempt, out_dir):
            print(f"Already exists in {out_dir}")

        run_ide(out_dir)


    def handle_verify(self):
        if self.move_node_directories():
            print("All nodes are in the right place!")
            

    def move_node_directories(self):
        lms_directory = Path.home() / "lms"

        rename_index = self.config.getint("setup", "rename_index", fallback=0)
        correct_paths, rename_index = self.request(f"/api/node-paths?rename_index={rename_index}")

        # Create a dict of all incorrectly structured directories
        misplaced = {}
        for path in glob("*/*/**/.git/", root_dir=lms_directory, recursive=True):
            assert path[-6:] == '/.git/'
            local_path = path[:-6]
            node_id = local_path.rsplit('/', 1)[1]
            if local_path.startswith("grading/"):
                continue

            correct_path = correct_paths.get(node_id)
            if correct_path and correct_path != local_path:
                local_path = lms_directory / local_path
                correct_path = lms_directory / correct_path
                if not correct_path.exists():
                    misplaced[local_path] = correct_path
        
        if len(misplaced) == 0:
            return True

        # Ask user if they want to restructure the incorrectly structured directories
        print("These directories are not in their recommended locations:")
        for local_directory, valid_directory in misplaced.items():
            print(f"  {local_directory} -> {valid_directory}")

        if not self.prompt_yes_no("Would you like to move them?"):
            return False

        for local_directory, valid_directory in misplaced.items():
            # Create leading path
            valid_directory.parent.mkdir(exist_ok=True, parents=True)

            # Move node directory
            shutil.move(local_directory, valid_directory)

            # Remove parent directory(s) if empty
            old_parent = local_directory.parent
            try:
                while True:
                    old_parent.rmdir()
                    old_parent = old_parent.parent
            except OSError: # Directory not empty
                pass

        self.config.set("setup", "rename_index", str(rename_index))
        with open(self.config_path, 'w') as file:
            self.config.write(file)

        return True
            

    @staticmethod
    def prompt_yes_no(prompt_msg):
        prompt_msg = f"{prompt_msg} [y/n] "
        answer = None
        while answer not in {'y', 'n'}:
            answer = input(prompt_msg).lower()
        return answer == 'y'


    @staticmethod
    def die(msg):
        print(f"Error: {msg}", file=sys.stderr)
        sys.exit(1)



VSCODE_SETTINGS_INITIAL = """{
    "update.mode": "none",
    "markdown-preview-enhanced.plantumlJarPath": "/usr/share/java/plantuml/plantuml.jar",
    "[python]": {
        "editor.tabSize": 4,
        "editor.insertSpaces": true
    },
    "errorLens.delayMode": "debounce",
    "errorLens.messageBackgroundMode": "message",
    "errorLens.statusBarColorsEnabled": true,
    "errorLens.statusBarIconsUseBackground": false
}"""


VSCODE_SETTINGS_FORCE = """{
    "ruff.lint.args": [
        "--select", "F,W,E,C90,I002,N,UP,YTT,ASYNC,BLE,B,A,COM818,C4,EXE,FA,G,PIE,Q002,Q003,RET,SLF,SIM,ARG,FIX,ERA,PL,FLY,PERF101,PERF102,PERF402,FURB148,FURB171,RUF006,RUF011,RUF016,RUF018,RUF019,RUF100,RUF200",
        "--ignore", "E226,E261,E401,E402,E501,PLR1714,PLR1722,PLR2004,PLW1514,SIM105,SIM107,SIM109,SIM110,SIM116,SIM117,W293",
        "--preview",
        "--target-version", "py311"
    ],
    "python.languageServer": "Pylance",
    "python.analysis.typeCheckingMode": "strict",
    "python.analysis.diagnosticSeverityOverrides": {
        "reportUnknownParameterType": "none",
        "reportUnknownArgumentType": "none",
        "reportUnknownLambdaType": "none",
        "reportUnknownVariableType": "none",
        "reportUnknownMemberType": "none",
        "reportMissingTypeArgument": "none",
        "reportMissingParameterType": "none",
        "reportMissingTypeStubs": "none",
        "reportUntypedFunctionDecorator": "none",
        "reportUntypedClassDecorator": "none",
        "reportUntypedBaseClass": "none",
        "reportUntypedNamedTuple": "none"
    },
    "errorLens.excludeBySource": [
        "cSpell",
        "Ruff(ERA001)",
        "Ruff(FIX002)"
    ]
}"""

EXTENSIONS = [
    "saxionsd.wiretext",
    "ritwickdey.liveserver",
    "shd101wyy.markdown-preview-enhanced",
    "charliermarsh.ruff",
    "ms-python.vscode-pylance",
    "ms-python.python",
    "streetsidesoftware.code-spell-checker",
    "usernamehw.errorlens",
    "njpwerner.autodocstring",
    "mechatroner.rainbow-csv",
]

SHELL_SETUP = f"""
#!/bin/sh

if command -v pacman > /dev/null && ! pacman -Qs visual-studio-code-bin > /dev/null ; then
    whiptail --yesno "Your teachers now recommend the use of Microsoft Visual Studio Code instead of Code OSS. Would you like to make the switch now?" 10 60 && (grep ^EnableAUR /etc/pamac.conf > /dev/null || pkexec sed -Ei '/EnableAUR/s/^#//' /etc/pamac.conf) && pkexec pamac install --no-confirm visual-studio-code-bin
fi

code {" ".join(" --install-extension "+ext for ext in EXTENSIONS)} > /dev/null

command -v poetry > /dev/null && poetry config virtualenvs.in-project true && rm -rf ~/.config/pypoetry/virtualenvs
"""

def setup_computer():
    subprocess.call(['sh', '-c', SHELL_SETUP])

    # Update VSCode settings
    if platform == 'darwin':
        vscode_path = Path.home() / "Library" / "Application Support" / "Code" / "User" / "settings.json"
    else:
        vscode_path = Path.home() / ".config" / "Code" / "User" / "settings.json"
    try:
        with vscode_path.open() as file:
            settings = json.load(file)
    except FileNotFoundError:
        settings = {}
        vscode_path.parent.mkdir(exist_ok=True, parents=True)
    settings = {**json.loads(VSCODE_SETTINGS_INITIAL), **settings, **json.loads(VSCODE_SETTINGS_FORCE)}
    with vscode_path.open("w") as file:
        json.dump(settings, file, indent=4)


if __name__ == '__main__':
    LmsCli(sys.argv[1:])
