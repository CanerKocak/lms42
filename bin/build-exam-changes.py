#!/bin/env python3

from glob import glob
import subprocess
import pickle
import html
import os

results = {}

for node_dir in glob('assignments/*-exam') + glob('assignments/*-project') + glob('assignments/graduation'):

    variants = {}
    for file in glob(f'{node_dir}/assignment*.yaml') + [f'{node_dir}/node.yaml']:
        cmd = ["git", "log", "--follow", "--patch", "--word-diff=porcelain", "--pretty=format:patch %cI %s", "--", file]
        proc = subprocess.run(cmd, capture_output=True, encoding='utf-8')
        patches = []
        for patch in proc.stdout[6:].rstrip("\n").split("\npatch "):
            lines = patch.split("\n")
            if ' ' not in lines[0]:
                continue
            date, description = lines[0].split(' ', 1)
            date = date[:10]

            if description in {'Git crypt restricted', 'Squashed all commits to remove exams from history', 'Programme periods can now have multiple groups. Happier, healthier, more effective.'}:
                continue

            changes = []
            meta = []
            for line in lines[1:]:
                if line[0:3] == '@@ ':
                    changes.append('')
                elif not line or line.split(' ', 1)[0] in {'index', '+++', '---', 'diff', 'similarity'}:
                    pass
                elif line[0] == ' ':
                    changes[-1] += html.escape(line[1:])
                elif line[0] == '+':
                    changes[-1] += f"<span class='new'>{html.escape(line[1:])}</span>"
                elif line[0] == '-':
                    changes[-1] += f"<span class='old'>{html.escape(line[1:])}</span>"
                elif line[0] == '~':
                    changes[-1] += "\n"
                else:
                    meta.append(line)
                    
            patches.append({
                "date": date,
                "description": description,
                "changes": changes,
                "meta": meta,
            })
    
        basename = file.split('/')[-1]
        if basename == "node.yaml":
            variant_name = "Common"
        else:
            variant_name = f"Variant {basename[10]}"
        variants[variant_name] = patches

    node_id = os.path.basename(node_dir)
    results[node_id] = variants
            

with open("exam-changes.pickle", "wb") as file:
    file.write(pickle.dumps(results))
