import os
import random
import pytest
from lms42.app import db
from lms42.models.user import User
from common import run_attempt

DISCORD_FILE_PATH = "/tmp/lms42-discord.txt"

@pytest.fixture(autouse=True)
def check_tests():
    random.seed(124) #sets the seed to 124 so that the same random numbers are generated every time (makes sure there wont be any needs_consent problems))

    if os.path.exists(DISCORD_FILE_PATH):
        os.remove(DISCORD_FILE_PATH)
    yield
    if os.path.exists(DISCORD_FILE_PATH):
        os.remove(DISCORD_FILE_PATH)

def set_discord_id(user_short_name, discord_id):
    """Sets the discord id of a user with a given short name."""
    user = User.query.filter_by(short_name=user_short_name).first()
    user.discord_id = discord_id
    db.session.commit()

    
def test_exercise_failed(client, _db):
    run_attempt(client, "vars", student_name="student1", grade="failed")
    assert not os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist


def test_exercise_passed(client):
    run_attempt(client, "html", student_name="student1", grade="passed")
    assert not os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist


def test_exam_passed(client):
    set_discord_id('student1', 1)
    run_attempt(client, 'pwa-project', student_name='student1', grade='passed')
    with open(DISCORD_FILE_PATH) as file:
        assert "passed" in file.read()


def test_exam_failed(client):
    set_discord_id('student1', 1)
    run_attempt(client, 'pwa-project', student_name='student1', grade='failed')
    with open(DISCORD_FILE_PATH) as file:
        assert "failed" in file.read()

def test_exam_results_disabled(client):
    set_discord_id('student1', 1)
    client.login("student1")
    client.post("/discord/notifications/exam_reviewed")
    
    run_attempt(client, 'pwa-project', student_name='student1', grade='passed')
    assert not os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist


def test_started_same_exercise(client):
    set_discord_id('student1', 1)
    run_attempt(client, 'css', submit=False, student_name='student1')
    run_attempt(client, 'css', submit=False, student_name='student2')
    with open(DISCORD_FILE_PATH) as file:
        assert "student2" in file.read()

def test_started_same_exercise_disabled(client):
    set_discord_id('student1', 1)
    client.login("student1")
    client.post("/discord/notifications/same_exercise")
    
    run_attempt(client, "html", student_name="student1")
    run_attempt(client, "html", student_name="student2")
    assert not os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist
