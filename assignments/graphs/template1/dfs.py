from mazes import maze1, maze2
from time import sleep

# Colors
FG_RED = '\x1b[31m'
FG_YELLOW = '\x1b[93m'
FG_RESET = '\x1b[39m'
# Change interval to speed or slowdown the drawing of the maze.
INTERVAL = 0.1

def solve(maze):
    """Finds a solution for the given `maze` from the top left square to the bottom right square,
    and mark the found path with 'o' characters. Returns a boolean indicating if a path was found.
    """

    height = len(maze)
    width = len(maze[0])
    origin = (0, 0) # (x,y)
    destination = (width-1, height-1) # (x,y)

    # You can use this `set` to prevent visiting the same positions twice
    # in the recurse function.
    visited = set()

    def visit(position):
        """This inner function (a function defined within a function) should be
        used recursively to visit a `position`."""

        # Mark on the maze that we have visited `position`:
        draw_on_maze(maze, position, '.')

        # TODO!

        # No path found.
        return False

    return visit(origin)


def draw_on_maze(maze, position, char):
    x, y = position
    maze[y] = maze[y][0:x] + char + maze[y][x+1:]
    print_maze(maze)

def print_maze(maze):
    sleep(INTERVAL)
    # Clean screen
    print('\033[2J')
    # Ansi escape sequences, for showing the path as red and dots as yellow
    maze_text = "\n".join(maze)
    path_len = maze_text.count("o")
    explored = maze_text.count(".")

    maze_text = maze_text.replace('.', FG_YELLOW+'.'+FG_RESET)
    maze_text = maze_text.replace('o', FG_RED+'o'+FG_RESET)
    print(f"Path length: {path_len}\nOther squares explored: {explored}\n{maze_text}\n")


if __name__ == '__main__':
    for maze_num, maze in [(1, maze1), (2, maze2)]:
        print(f"----- Maze {maze_num} -----\n")
        if solve(maze):
            print_maze(maze)
        else:
            print("No path.\n\n")
