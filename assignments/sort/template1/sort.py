import random
import time

def custom_sort(data : list):
    """Sorts the items in the `data` list in-place. Returns nothing."""
    # TODO
    pass


def selection_sort(data: list):
    """Sorts the items in the `data` list in-place. Returns nothing."""
    # TODO
    pass


def merge_sort(data: list):
    """Sorts the items in the `data` list in-place. Returns nothing."""
    # TODO
    # In this case, you may have to create copies of (part of) the list.
    pass


def quick_sort(data: list):
    """Sorts the items in the `data` list in-place. Returns nothing."""

    # For performance and ease of implementation, it's probably a good idea to create
    # a separate function that takes a `start` (inclusive) and an `end` (exclusive),
    # between which indexes it will sort the list.
    # The `quick_sort(data)` function itself is only here to set the default values
    # such that the entire list is sorted.
    quick_sort_recurse(data, 0, len(data))

def quick_sort_recurse(data: list, start: int, end: int):
    # TODO
    # You *DO NOT* need to create copies of (part of) the list.
    pass



if __name__ == '__main__':
    # Use this to play around with your sort algorithms.
    data = [random.randint(0, 999999) for _ in range(10)]
    
    start_time = time.perf_counter()
    custom_sort(data)
    print('Time: %.3fs' % (time.perf_counter() - start_time,))

    print(data)
