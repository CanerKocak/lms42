name: Layouts
description: Implement the user interface for an app.
goals:
    android-layouts: 1
resources:
    - In this lesson, we'll study how to create (pretty) layouts in Android. The following resources should provide you with more than enough pointers to get started. (There's no need to work through all of them, as they mostly cover the same subjects.)
    -
        link: https://www.raywenderlich.com/9193-constraintlayout-tutorial-for-android-getting-started
        title: "ConstraintLayout Tutorial for Android: Getting Started"
    
    -
        link: https://www.raywenderlich.com/9475-constraintlayout-tutorial-for-android-complex-layouts
        title: "ConstraintLayout Tutorial for Android: Complex layouts"

    - 
        link: https://www.youtube.com/watch?v=h1LHzObflwo
        title: Building a layout from Scratch using ConstraintLayout
        info: A video showing how a pretty complex layout it created. It contains a lot of information and practical tips. It's a bit long though, so we recommend try to crank the playback speed up as much as you can absorb. And of course, feel free to skip parts if you think you get it.

    -
        link: https://constraintlayout.com/basics/
        title: ConstraintLayout.com - Basics
        info: Everything you'll want to know about ConstraintLayouts, and a bit more.

    -
        link: https://www.youtube.com/watch?v=ynOUzHFFMeg
        title: Styles and Themes
        info: Styles are like CSS classes. Themes allow you to configure things like colors in a single place. This video explains.

assignment:
    Bank app:
        - |
            <img src="portrait.png" style="width: 300px; float: left; margin-right: 16px;"><img src="landscape.png" style="height: 300px; float: left;">
            <div style="clear: both;"></div>

            You might recognize the screenshots above. They are from the app of a large Dutch bank. Your job? Recreate this screen as closely as you can using an Android layout! Tapping buttons doesn't need to do anything and data can be hard-coded. Just make this one screen look good!

            (In case you're wondering how Frank managed to obtain such an easy to remember IBAN number: the screenshots are not from the actual bank app, but from a pretty faithful recreation by yours truly.)

        - A few additional requirements: |
            - You should be able to demo your layout on an actual device (or emulator), not just in the layout editor.
            - Your layout needs to stretch properly to any common screen size (including landscape mode). Take a look at the landscape screenshot for an example.
            - You should change the primary color in the theme to match the bank's house style (#00857a). When defining Views, you should only use colors from the theme, so that they can be changed in a single location.
            - You should extract *styles*, to prevent having to repeat lists of common View properties over and over.

        - A few hints: |

            - The application uses the standard material design icons. You can add one to your project through *File &rarr; New &rarr; Vector Asset*. When the asset type is *Clip Art* you can click the icon next to *Clip Art:* to select one from the library. If you can't find the exact icon you need, just use something vaguely similar.
            - The colored images next to the bank accounts are provided in the template. They can be imported as vector assets.
            - `TextView`s have a `drawableRight` attribute, that can be used to add an image to them. This may make it simpler to implement the *Direct service* buttons.
            - You can start creating a style by first applying the appropriate attributes to a single *View*, selecting that *View* in the design editor and then *Refactor &rarr; Extract &rarr; Style*.
            - The default theme is provided in `res/values/themes/themes.xml`.
            - If you really don't know how to proceed, you may want to take some inspiration from the example View hierarchy below. Note that this tree represents just *one* way to do this - other solutions may also be great!
            
        - Example View hierarchy: |
            <img src="layout-tree.png" style="display: block; margin-top: 8px;" class="no-border">
        
        -
            text: Your layout should look (exactly) like the portrait screenshot.
            0: No styling and significant missing parts.
            1: No styling or significant missing parts.
            2: Mostly right, but one or two show-stopping details.
            3: A few small differences, but nothing ABN couldn't release.
            4: Almost pixel perfect.
            weight: 3
        -
            text: Your layout should stretch to fit any screen size, resembling the landscape screenshot.
            0: No stretching at all.
            2: Mostly right, but one or two obvious flaws.
            3: One tiny problem, barely visible.
            4: Stretches like a ballerina.
        -
            ^merge: codequality
            text: Your layout should be maintainable through the uses of *styles* and the *theme*.
            malus: 0
            bonus: 0
