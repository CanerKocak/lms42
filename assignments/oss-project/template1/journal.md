# OSS project journal

## Monday 2021-01-01
- Accomplished:
  - Finished writing tests for the flux capacitor widget. Test coverage is now at about 80%, but that seems to cover all complex cases.
  - Created first design sketches for the mobile fusion reactor. This is a bit harder expected. I'll continue sketching tomorrow, but if this doesn't work out, we can always choose to go for an ordinary molten salt reactor.
- Learned:
  - The theoretical basics of nuclear fusion. This is still pretty abstract.
- Open questions and next steps:
  - How are fusion reactors built in practice?
  - How can incredibly hot plasma be contained without melting the container?
  - Are there any simple approached that can be follow for miniaturizing large machines?  
