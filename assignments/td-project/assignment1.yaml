Entry conditions:
    -
        title: Class diagrams
        text: Your grade should be at least a 7.5.
        must: true
        map:
            class-diagrams: 1
    -
        title: Design patterns
        text: Your grade should be at least a 7.5.
        must: true
        map:
            design-patterns: 1


The C4 model:
- |
    You'll be using the C4 model for creating an graphical overview of your technical design.
-
    link: https://c4model.com/
    title: The C4 model for visualising software architecture

-
    link: https://purrgramming.life/cs/c4-plantuml/
    title: Introduction to C4-PlantUML with Examples
    info: C4-PlantUML combines the benefits of PlantUML and the C4 model for providing a simple way of describing and communicate software architectures – especially during up-front design sessions – with an intuitive language using open source and platform independent tools.

-
    link: https://github.com/plantuml-stdlib/C4-PlantUML
    title: GitHub - C4-PlantUML
    info: The official C4-PlantUML documentation.


Case description: |
    *The following case description consists of a random selection from various sets of options. Some combinations may be difficult to interpret, in which case you may ask a teacher for clarification.*

    [context:tennis]{Create the technical design for a management system for tennis clubs, which will be offered as *Software as a Service*. Users can be divided into system administrators (who manage all clubs and the system as a whole), club administrators (who manage courts, members, events, etc) and club members (who can book courts, register for events, etc).}
    [context:expertise]{Create the technical design for an employee expertise database, for use in huge companies. It will be offered as *Software as a Service*. Employees can manage their skill listings, search for employees with skills, and broadcast and respond to questions. Company administrators can moderate all content. System administrators can manage company accounts.}
    [context:preschool]{Create the technical design for a preschool (nursery/creche) progress monitoring system, which will be offered as *Software as a Service*. It allows employees to track various metrics for each child, and to share messages and media with parents. Parents should be able to view all information about their children. School administrators can manage all settings and roles within their school. System administrators can manage the list of schools.}

    [login:external]{- An external identity provider should be used for logins.}
    [login:internal]{- Administrators should be able to add user accounts. Users should be able to login and to reset their password by email.}
    [login:pre]{- The company has a pre-existing user management and login system that this application should use.}

    - The system should provide an API that allows third party applications to perform some basic operations and to receive real-time updates from our system on behalf of an end user.

    [platform:apps]{- Users should be able to download an app with the functionality in the iOS App Store and the Android Play Store.}
    [platform:cross]{- Users should be able to use the app on their phones and on their laptops.}
    [platform:laptop]{- Users only need to be able to use the app on their Windows/Apple laptops.}
    [platform:iOS]{- Users only need to be able to use the app on their iPhones.}

    [quality:low]{- The product needs to be delivered as fast and cheap as possible. The end-user experience may suffer a bit.}
    [quality:medium]{- The product needs to strike a balance between being polished and inexpensive.}
    [quality:high]{- The end-user experience needs to look and work very polished.}

    [admin:free]{- Administrator functionality does not need to be as polished as end-user functionality. It may be offered through a separate platform.}
    [admin:integrated]{- All functionality, including administrator function, should be offered on all supported platforms.}

    [integration:social]{- Users need to be able to share some data/achievement on a popular social medium.}
    [integration:payment]{- Something in the system requires payment, which users should be able to make online.}
    [integration:bot]{- The system can be interacted with through a bot on a popular chat platform.}

    [media:video]{- The system must support uploading, storage and viewing of large amounts of video files, which are impractical to store in a regular database or on the backend server.}
    [media:video]{- After uploading, videos must be re-encoded into convenient streaming formats for various platform.}
    [media:photo]{- The system must support uploading, storage and viewing of large amount of photos, which are impractical to store in a regular database or on the backend server.}
    [media:photo]{- After uploading, photos must be run through a face recognition system, to tag known end users (who have consented to this).}
    
    [realtime:chat]{- Users should be able to be able to chat with each other and see message appear in the app/site in real-time.}
    [realtime:video]{- Users should be able to engage in one-on-one live video calls through the platform.}
    [realtime:push]{- Users should be able to receive notifications when something happens while they don't have the app/site opened.}
    [realtime:live]{- Some of the data shown in the app/site should be updated as-it-happens.}

    [exp1:kotlin]{- The team has experience with Kotlin.}
    [exp1:django]{- The team has experience with Django.}
    [exp1:ror]{- The team has experience with Ruby on Rails.}
    [exp2:js]{- The team has experience with Javascript.}
    [exp2:swift]{- The team has experience with Swift.}
    [exp2:dotnet]{- The team has experience with .NET and C#.}
    [exp3:mongo]{- The team has experience with MongoDB.}
    [exp3:mysql]{- The team has experience with MySQL.}
    [exp3:firebase]{- The team has experience with Firebase.}
    [exp4:aws]{- The team has experience with AWS (Amazon Web Services).}
    [exp4:gcp]{- The team has experience with the Google Cloud Platform.}
    [exp4:self]{- The team has experience with managing their own dedicated servers.}
    - The team is of course willing and able to learn other technologies.


Helicopter view:
-
    title: Level 1
    map:
        architecture: 1
        plantuml: 1
    text: |
        Create a C4 level 1 model for the system described above. Details such as technologies, protocols and the names of specific services and cloud offerings can be left out at this stage.
    4: Perfect


Doing research:
- |
    The following parts of this project will involve doing a lot of research. You'll need to search for alternatives, and learn about them, weigh their pros and cons, and then decide. Expect to be Googling, reading and learning about 80% of your time in this project.

    Here are some useful resources for coming up with alternatives:
-
    link: https://alternativeto.net/software/flutter-dart/
    title: AlternativeTo - Flush Alternatives
    info: This website is one way to explore alternative technologies to use, if you know the name of one suitable technology. Keep it mind that it may not help you think outside-the-box.
-
    link: https://www.elegantthemes.com/blog/wordpress/tools-to-see-what-software-a-website-was-built-with
    title: 4 Tools to See What Software a Website Was Built With (And Why You’d Want To)
    info: Another way to come up with alternatives is to try to learn what technology similar (successful!) apps are using. This page lists some tools that may help you do that.
- |
    And of course you'll use your favorite search engine to come up with alternatives. Try to use various search terms, thinking outside-the-box. So instead of just searching for `flask alternative`, see what hits you get when you're more specific `python api framework` or `rapidly build python backend`. Also, don't forget that a cloud service (such as Google Firebase), as opposed to an (open source) product (such as MongoDB), *may* be the best alternative for a particular situation.

    Here are some techniques and tips to help you make and motivate decisions:
-
    link: https://www.youtube.com/watch?v=hbMrnQiZkEw&t=65s
    title: 5 tips for how to make Technical Decisions as a Software Engineer
-
    link: https://medium.com/swlh/decision-management-in-software-engineering-ca60f9d40e02
    title: Decision Management in Software Engineering
-
    link: https://toggl.com/blog/decision-matrix
    title: Need to Make a Tough Decision? A Decision Matrix Can Help
-
    link: https://www.decisionskills.com/uploads/5/1/6/0/5160560/worksheet_-_weighted_pros_and_cons.pdf
    title: Weighted pros and cons - Worksheet
    info: The *weighted pros and cons* technique can help you make a yes/no decision.
- |
    Exploratory research like you'll be doing for the rest of this assignment has no end. There's always more to explore and more to know about the technologies you're evaluating. Therefore, we recommend that you employ [timeboxing](https://www.youtube.com/watch?v=kZYjchCf68s). We'll provide you with a recommended timebox for each objective.

Basic architecture:
- |
    At first, we'll be designing the system from the *case description* based on the assumption that the company is a startup. We're only planning ahead to have a couple of thousand end users (which is not many given the computing power a single server can offer) and we want to keep things simple and relatively inexpensive.

-
    title: Level 2
    text: |
        *Recommended timebox*: 2½ days.

        Copy your C4 level 1 model and create a C4 level 2 model out of it.

        - Decide how many and which containers and databases you'll be using, and add them to your model as part of the *System*. (Apps? Web sites? Backends? Databases? Background services?)
        - Model the relationships from/to your containers.
        - For each container, database or external service, choose the technology to use, and add it to the model. Some examples: Python/Flask, Ruby/Rails, PHP/Laravel, Native Android app, Flutter app, MongoDB database, Amazon S3, Amazon Lambda, Amazon SES email service, Stripe payment service, etc, etc. Motivate your decisions, by listing the pros and cons for your chosen technology as well as for alternative choices. The template already contains a partial example you can extend. But feel free to pick another format and/or change any items. *Hint:* install the *Markdown Table* Visual Studio Code extension by *TakumiI*, in order to edit Markdown tables without losing your sanity.
        - For all relationships (except those involving a *Person* on one end), decide on the protocol to use, and add it to the model. When using a pre-existing protocol (such as when interfacing with an external service), you provide a link to its documentation. When there are multiple options, motivate your decision like above. (For example: HTTP/REST, WebSocket, GraphQL, SMTP, TCP, MQTT, gRPC.)
        
    scale: 10
    map:
        architecture: 1
        decisions: 3
        plantuml: 1
        decomposition: 1
    4: States only the most well-known techniques that seem to almost fit, without viable alternatives or reasoning.
    6: Slightly disappointing for a junior. Partially flawed reasoned, wrong understanding everywhere, in most cases only one viable alternative was stated.
    8: Good for a junior. Sound reasoning, some wrong understanding, most choices were made between at least two viable alternatives.
    10: Good for a medior.


Scalable architecture:
- |
    *Recommended timebox*: 1 day.

    Our startup company is doing extremely well! A new architecture is needed that will support a 100 million end users.

    *Some general advice*: don't worry about making your systems scalable to arbitrary scales when you don't foresee a short-term need for it. Creating a highly scalable usually adds a *lot* of complexity, keeping you from creating the things the customer actually wants. But in this case, let's go crazy!

-
    text: |
        Copy your C4 level 2 model from the previous objective, and make any changes you need that will allow it to scale to a 100 million end users. (That number is just there to get a sense of what scale we're talking about. We don't expect you to do any calculations based on that.) If you mean for a container to have as many (cloud) instances as needed, you may just add `(autoscaling)` to the technology label. You may also want to introduce load balancers, add a message bus or message broker (for instance for real-time communication between autoscaling instances), or replace technologies with automatically scaling alternatives. 

        Explain below the model what changes you made and why (or why not). You *don't* need to evaluate multiple alternatives for all choices this time; explaining the pros and cons of your choices option relative to your original solution is enough. Refer to documentation (of scalability claims) where applicable.
    scale: 10
    map:
        scalability: 1
    4: 
    6: Slightly disappointing for a junior.
    8: Good for a junior. Something mostly plausible, if not entirely correct.
    10: Good for a medior.


Reliability:
- |
    When you start horizontally scaling your system, it will be running on more and more computers. So the chance of one of these computers failing catastrophically increases. But we want the system as a whole to be reliable, of course. No computer may be a single point of failure.
-
    text: |
        *Recommended timebox*: 4 hours.

        Explain, for each of your containers, how a single broken server will not cause your system to go down (for long) and (where applicable) will not cause (much) data loss. Refer to documentation (of reliability claims) where applicable.
    scale: 10
    map:
        reliability: 1
    4: 
    6: Slightly disappointing for a junior.
    8: Good for a junior. Something mostly plausible, if not entirely correct.
    10: Good for a medior.


Security:
-
    text: |
        *Recommended timebox*: 4 hours.

        Explain for each of your *scalable architecture* relationships how it is secured. What authentication scheme is in use? Or is there network-level security? Is/should the connection be encrypted? Is that part of the used protocol? Refer to documentation where applicable.
    scale: 10
    map:
        secure-design: 1
    4: 
    6: Slightly disappointing for a junior.
    8: Good for a junior. Something mostly plausible, if not entirely correct.
    10: Good for a medior.


Backend components:
-
    text: |
        *Recommended timebox*: 1 day.

        Create a C4 level 3 model for your (most complex) backend container, by splitting it up into a couple of mostly independent components. You should add relationships between the components, describing the sort of interaction they have.
        
        Explain the pros and cons for creating microservices out of these components, as opposed to keeping them together as a single (monolith) application. Make and motivate your decision.

        If you were to use microservices, what communication protocol(s) would be used between them? How are the different services able to find one another?
    scale: 10
    map:
        decisions: 1
        decomposition: 2
    4: 
    6: Slightly disappointing for a junior.
    8: Good for a junior. Something mostly plausible, if not entirely correct.
    10: Good for a medior.


Splitting up further?:
- |
    C4 level 4 consists of class diagrams. You've already learned how to use and design them. We won't ask you to create one here, as they require a far greater level of detail then you've been provided with in this case study (and it would be an awful lot of work, given the size of the system under design).
