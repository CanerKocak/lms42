# Network Address Translation 

In this objective you will be presented with an overview of a network and asked to answer some questions. Provide your answers in this file.

## Questions

Suppose Bob uses his laptop to request a HTML page from a web server on the internet. In the diagram below we are assuming an ip range of 10.0.0.0/24 for Bob's LAN with the provided IP addresses. 
   

1. Assume Bob want to request a HTML page from the web server. Which (IP) addresses and (TCP) port numbers are used to create the request on his laptop? You can assume that the web server uses HTTPS (which determines the port used by the web server). Fill in the table below:

| Source address | Source port | Destination address | Destination port      |
| -------------- | ------------------- | ---------- | ------------ |
| ...            | ...                 | ...      | ... |



2. The source port is assigned by the operating system of Bob's laptop. In which range does this source port number fall?

...


3. Explain why the source port is included in the request created on Bob's laptop.

...



4. Assume that Bob also requests the same page from the web server on his phone. The default gateway has a NAT table which stores mapping between the internal and public addresses. Assume the default gateway uses PNAT. Fill in the NAT table in the overview (replace the '...' with your answer). You are free to choose the port numbers as long as they are valid port numbers.

5. What would happen if both Bob's laptop and Bob's phone happen to pick the same source port for setting up this connection?

...


6. Bob's request from his laptop is translated to a new request at the default gateway. Which (IP) addresses and (TCP) port numbers are used to in this request? Fill in the table below:

| Source address | Source port | Destination address | Destination port      |
| -------------- | ------------------- | ---------- | ------------ |
| ...            | ...                 | ...      | ... |



```plantuml
@startuml 
!define osaPuml https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master
!include osaPuml/Common.puml
!include osaPuml/User/all.puml
!include osaPuml/Hardware/all.puml
!include osaPuml/Misc/all.puml
!include osaPuml/Server/all.puml
!include osaPuml/Site/all.puml

package "Local Area Network (LAN)" {
  

' Users
together {
osa_user_green(Bob, "Bob", "User", "")
}

' Devices
together {
osa_laptop(laptop, "IP: 10.0.0.100", "MAC: C8-00-84-E7-36-28", "Bob's Laptop")
osa_iPhone(phone, "IP: 10.0.0.200", "MAC: C8-69-CD-4C-0A-0D", "Bob's Phone")
}

' Network
osa_device_wireless_router(wifiAP, "Public IP: 145.2.205.11", "MAC: 8E:9C:1f:8E:5E:16", "Default gateway")

note left 
    NAT table
    | **Internal address and port**   | **Public address and port** |
    | ... | ... |
    | ... | ... |
end note
}


package "Internet (WAN)" {
    together {
        osa_database(cloud, "ISP Router", "ISP")
        osa_database(router1, "Router", "Network")
        ' osa_database(router2, "Router 2", "Network")
        ' osa_cloud(cloud, "Internet", "Network")
        osa_server(server, "178.21.117.25", "Ubuntu Server 20.04 LTS", "Hosting Provider")
    }
}

Bob ..> laptop: works on
Bob ..> phone: messages on

laptop--> wifiAP
phone --> wifiAP

wifiAP <--> cloud
router1 <-r-> cloud

server <-r-> router1


@enduml
```
