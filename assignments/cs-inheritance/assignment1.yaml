Introduction:
    - |
        You may have noticed while testing that our game is getting a little bit.... easy to win, since we have given the player mighty weapons to smash around the monsters, while the monsters have been given nothing to counter that. In this assignment, which will handle inheritance, we are going to change that.
    -
        link: https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/inheritance
        title: Inheritance
        info: This article explains the basics of inheritance in C#.

    -
        link: https://www.geeksforgeeks.org/c-sharp-inheritance/
        title: Inheritance
        info: A different article explaining the same concepts.
    
Introducing Characters:
    - |
        First of all, let's start with what we have. You may have noticed that some of the code in the `Monster` and `Player` classes are quite similar. We can put a base class on top that captures this common functionality and only keep the Monster and Player-specific logic in the subclasses.

    - Character Class: 
        -   text: |
                Introduce a class `Character` from which `Player` and `Monster` should inherit. Make sure as much methods and properties are in the base class as possible while keeping original functionality.
                Note that in order to access properties and methods from a base class you should mark them as protected instead of private. For more information, refer back to the docs in the previous assignment which introduces encapsulation.

                Also note the `ToString` method we implemented earlier. It has the override keywoard on it. This simply means that the implicit base class (all classes have a base class by default) has a `ToString` method as well and with the override we change the implementation for this specific subclass.
            ^merge: feature
            map:
                inheritance: 1

    - Refactoring fun:
        -   text: |
                Next, we will change some of the functionality or technical implementation slightly in order to make Players and Monsters more similar.
                - A monster should get a proper name and not just a number. For now, you can make something up as long as it's unique among all monsters.
                - Monster gets a method to heal, exactly the same how a Player would heal, allowing you to move this logic into the base class as well.
                - An attacking monster should no longer use it's number to determine it's damage (this makes no sense). For now, let it deal a random number of damage between 5 and 15. This will be updated shortly, as this is a big part of our monster buff.
                - Similar to how players interact, monsters should also get the ability to Heal. It just determines randomly whether it will attack or heal on a turn.
                - Implement a method `Character.Attack` but you can just throw an exception for now. You should mark it as virtual, in the same way you use the override keyword. Let the Attack method of `Monster` and `Player` override this method, similar to how we did this with `ToString`. In a future assignment, you will learn of a better way of doing this using abstract classes. This will allow other classes to call the `Attack` method on any implementation of Character without knowing what subclass it's dealing with.
            ^merge: feature
            map:
                inheritance: 1

    - Multiple Monsters with special attacks:
        -   text: |
                Now let's add some monsters! There are 6 different types of monsters (indicated 1 through 6), but they only have 3 different types of behaviors. These 3 behaviors should be implemented as classes (MultiplierMonster, ExponentialMonster, ConstantMonster) all inheriting from the Monster class.

                Each of the 6 monster types should be implemented as classes, inheriting from the appropriate behavior classes.
                
                | id | Name | Description | Health | Behavior |
                | -- | -- | -- | -- | -- |
                | 1 | dungeon cat | Dungeon cats are not actually cats. They're way too big for that. And they look more like giant hamsters. They do live in the dungeons though. | 10 | multiplier 1 |
                | 2 | common house dragon | Whoever invented that name probably never met one. | 25 | multiplier 3 |
                | 3 | cursed dragon | Fighting a cursed dragon can be the easiest thing you do today. Easier than brushing your teeth. Or it can be the hardest (and last) thing you'll ever do. | 15 | exponential 2 |
                | 4 | orc warrior | These nimble but incredibly skilled fighters have basically invented war. | 30 | constant 20 |
                | 5 | vampire dragon | Exactly like two strong warhorses. But flying. And breathing fire. | 45 | constant 30 |
                | 6 | magma elemental | Unknown. We're not even sure they exist. | 50 | exponential 3 | 

                Here is how the different behaviors work:
                
                | Behavior | Description |
                | -- | -- |
                | multiplier | The damage it does is determined by a single dice (1-6), multiplied by *the multiplier value for this monster type*. |
                | exponential | The damage it does is determined by a single dice (1-9), to the power of *the exponent value for this monster type*. |
                | constant | It always does the exact same damage, depending on the *damage value for this monster type*. |

            ^merge: feature
            map:
                inheritance: 1
            