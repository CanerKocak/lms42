name: Room
goals:
    android-database: 1
days: 2
resources:
    Some important background info:
    -
        link: https://www.youtube.com/watch?v=gvLMPHiAK7M&list=PLUCqzUmT6vrgh8YxZUxwQ1gV8HNjPdnEd&index=1
        title: "A practical introduction to Android Room - 1: Jetpack"
        info: What's Jetpack/androidx, and what types of functionality does it offer?
    -
        link: https://developer.android.com/jetpack
        title: Android Developers - Jetpack    
    -
        link: https://www.youtube.com/watch?v=AsvnD3nQZzg&list=PLUCqzUmT6vrgh8YxZUxwQ1gV8HNjPdnEd&index=2
        title: "A practical introduction to Android Room - 2: Android Architecture Components"
        info: A broad overview of the Android Architecture Components, that are now part of Jetpack.
    -
        link: https://developer.android.com/jetpack/guide#overview
        title: "Android Developers - Jetpack - Guide to app architecture - Recommended app architecture"

    Android Room:
    -
        link: https://www.youtube.com/watch?v=RstQg7f4Edk&list=PLUCqzUmT6vrgh8YxZUxwQ1gV8HNjPdnEd&index=3
        title: "A practical introduction to Android Room - 3: Entity, DAO and Database objects"
        info: In this coding session, we'll be creating our first Room Entity (table), Data Access Object (DAO), and Database. For now, we'll just insert a row and print all existing rows in MainActivity.onCreate.

    -
        link: https://developer.android.com/training/data-storage/room/#java
        title: Android Developers - Docs - Guides - Overview

        link: https://developer.android.com/training/data-storage/room/defining-data
        title: Android Developers - Docs - Guides - Defining data using Room entities

        link: https://developer.android.com/training/data-storage/room/accessing-data
        title: Android Developers - Docs - Guides - Accessing data using Room DAOs


assignment:
    -
        ^merge: feature
        code: 0.2
        weight: 1
        text: |
            Replace the static data store in your cocktail app with a Room database!

            For now, you can just use main-thread queries. (So no need for `Thread`s or `LiveData` yet.) Also, don't worry if your `Activity`s don't (always) update to show the latest data. We'll fix that in the next objective.

    -
        link: https://www.youtube.com/watch?v=CNz4ZA68-HA&list=PLUCqzUmT6vrgh8YxZUxwQ1gV8HNjPdnEd&index=4
        title: "A practical introduction to Android Room - 4: LiveData and threading"
        info: Let's make an actual application out of the experiment we did in the last video, by creating a functional user interface. We'll find out how to automatically update views based using LiveData, and how to use threads to prevent the UI becoming unresponsive.

    -
        link: https://www.youtube.com/watch?v=pjyBvI37c5A&list=PLUCqzUmT6vrgh8YxZUxwQ1gV8HNjPdnEd&index=5
        title: "A practical introduction to Android Room - 5: Joins and the Database singleton"
        info: We'll extend our app to include an extra Entity (table) and an additional Activity that requires access to the AppDatabase object, which we'll create as a singleton. Furthermore, we want our main list of items to show the results of a join, requiring us to create an object tailored to hold the query results for this.

    - |
        <div class="box" style="display: flex;">
            <div style="margin-right: 8px; flex: 1;">
                In case you'd like to view the resulting source code from the <em>A practical introduction to Android Room</em> housing example, you can download it here.
            </div>
            <a href="funda-app.tar.gz" class="button">Housing app</a>
        </div>

    -
        ^merge: feature
        code: 0.2
        weight: 1
        text: |
            Make sure your application can only instantiate a single AppDatabase object by creating a *singleton* out of it. Also, modify it to disallow main-thread queries.

            Fix your application by doing database updates in `Thread`s and by doing all database reads using `LiveData`. This should not only make your application more responsive (as database queries are no longer blocking user interactions), but should also make it easy to always present the latest data to the user, no matter what caused the updates.

    -
        link: https://developer.android.com/training/data-storage/room/relationships
        title: Android Developers - Docs - Guides - Define relationships between objects

    - |
        <video muted autoplay loop class="side" style="max-height: 500px;" src="room-demo.webm"></video>Let's add some additional functionality to the app. The desired functionality is described in the objectives below. The end result is shown in the screen capture.

    -
        ^merge: feature
        code: 0.2
        text: |
            Allow the user to add reviews on a cocktail's *view* activity. The reviews should be displayed in that same activity.

            **Hints:**
            
            To get the entire page to scroll, instead of just the list of reviews, the top-level `ConstraintLayout` can be wrapped inside a `ScrollView`, like this:


            ```xml
            <?xml version="1.0" encoding="utf-8"?>

            <ScrollView
                xmlns:android="http://schemas.android.com/apk/res/android"
                xmlns:app="http://schemas.android.com/apk/res-auto"
                xmlns:tools="http://schemas.android.com/tools"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:fillViewport="true"
                >

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    tools:context=".CocktailViewActivity">
            ```
            
            As the list of reviews shouldn't scroll by itself, it probably shouldn't be a `ListView`. Instead, it can be a vertical `LinearLayout`, a trivially simple Android layout that displays `View`s underneath each other. You can instantiate your review instances using a `LayoutInflater`, and add them to the `LinearLayout`. When the reviews are updated and need to be refreshed, all existing reviews can be removed using `removeAllViews()`.

    -
        ^merge: feature
        code: 0.2
        text: |
            In the `MainActivity`, show how many reviews each of the cocktails have. Perform the count using an SQL join or a subquery, such that you don't need to do an additional query per cocktail.

            *Hints:*
            - Create a subclass of your `Recipe` database class called `RecipeWithReviewCount`. It should have an extra `reviewCount` field, that will hold the result of the additional column in the query. As this field is in a subclass, and not in the database entity itself, it will never be stored to disk.
            - Within your DAO, write a join-query that delivers a (observable) list of `RecipeWithReviewCount` objects.
            - Within your `Recipe` list adapter, use `instanceof` to see if the `Recipe` is in fact a `RecipeWithReviewCount`, in which case you can cast it and set the review count text to the appropriate value. Otherwise, you can set the text to an empty string.

    -
        link: https://stackoverflow.com/questions/47511750/how-to-use-foreign-key-in-room-persistence-library
        title: How to use foreign key in Room persistence library

    -
        ^merge: feature
        weight: 0.5
        code: 0.2
        text: |
            Allow the user to *delete* a cocktail from the ActionBar in the cocktail's *view* activity. Any associated review should be deleted as well. The latter can be done using a separate query, or (ideally) by defining a foreign key with a cascading delete policy.
